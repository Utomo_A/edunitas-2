package com.example.utomoardy.edunitas.module;

import com.google.firebase.database.ServerValue;

/**
 * Created by windows on 3/9/2018.
 */

public class Post {
    public String fullname;
    public String content;
    public String photo;
    public long time;
    public Object timestamp;
    public int sumLove;
    public int sumComment;
    public String photoprofile;

    public Post() {
    }

    public Post(String fullname, String content, String photo, int sumLove, int sumComment, String photoprofile) {
        this.fullname = fullname;
        this.content = content;
        this.photo = photo;
        this.sumLove = sumLove;
        this.sumComment = sumComment;
        this.timestamp = ServerValue.TIMESTAMP;
        this.photoprofile = photoprofile;
    }
}
