package com.example.utomoardy.edunitas.module;

/**
 * Created by Utomo Ardy on 17/03/2018.
 */

public class Distance {
    public String text;
    public int value;

    public Distance(String text, int value) {
        this.text = text;
        this.value = value;
    }
}
