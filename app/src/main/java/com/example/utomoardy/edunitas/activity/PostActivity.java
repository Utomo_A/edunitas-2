package com.example.utomoardy.edunitas.activity;

import android.Manifest;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.example.utomoardy.edunitas.R;
import com.example.utomoardy.edunitas.module.Post;
import com.example.utomoardy.edunitas.module.ProfilePost;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.OnProgressListener;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;
import java.util.UUID;

import static android.provider.MediaStore.Files.FileColumns.MEDIA_TYPE_IMAGE;

public class PostActivity extends AppCompatActivity {


    private static final int MY_PERMISSIONS_REQUEST_CAMERA = 1;
    private static final int MY_PERMISSIONS_REQUEST_READ_EXTERNAL_STORAGE = 2;
    private static final int MY_PERMISSIONS_REQUEST_WRITE_EXTERNAL_STORAGE = 3;

    AlertDialog dialog;

    private static final String TAG ="PostActivity" ;
    EditText mEtWrite;
    ImageView mFoto, mCamera;
    TextView tvTambahFoto;
    Spinner mSpinner;

    FirebaseAuth mAuth;
    FirebaseDatabase mDatabase;
    DatabaseReference mRoot, mPost, mUser, mSchool, mProfile;
    StorageReference storageReference;
    FirebaseStorage mStorage;

    String caption;
    String mFullname ;
    String photoUrl;
    String pilih;

    private static final int RC_OPEN_GALLERY = 102;
    private static final int RC_TAKE_PHOTO = 101;

    static String IMAGE_DIRECTORY_NAME = "Edunitas";
    private Uri mFileUri;
    Uri file;

    ListView listView = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_post);

        mAuth = FirebaseAuth.getInstance();

        permission();

        listView = new ListView(this);
        String[] items = {"Camera","Gallery"};
        ArrayAdapter<String> adapterCamera = new ArrayAdapter<String>(this, R.layout.cameraorgallery, R.id.txtitem, items);
        listView.setAdapter(adapterCamera);

        mStorage = FirebaseStorage.getInstance();
        storageReference = mStorage.getReference();

        mEtWrite = findViewById(R.id.etWriting);
        mFoto = findViewById(R.id.ivHasilFoto);
        mCamera = findViewById(R.id.ivCamera);
        tvTambahFoto = findViewById(R.id.tvCamera);
        mSpinner = (Spinner)findViewById(R.id.spinner_content);
        String[] list = {"public", "private"};
        Log.d(TAG, "list :"+list);
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_dropdown_item, list);
//        ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(PostActivity.this,
//                R.array.list_post_status, R.layout.item_post_status);
//        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        Log.d(TAG, "adapter :"+adapter);
        mSpinner.setAdapter(adapter);


        mDatabase = FirebaseDatabase.getInstance();
        mRoot = mDatabase.getReference();
        mPost = mRoot.child("posts");
        mUser = mRoot.child("users");
        mSchool = mRoot.child("schools");
        mProfile = mRoot.child("profileposts");

        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                ViewGroup vg = (ViewGroup) view;
                TextView txt = (TextView) vg.findViewById(R.id.txtitem);
                //Toast.makeText(PostActivity.this,txt.getText().toString(),Toast.LENGTH_LONG).show();
                pilih = txt.getText().toString();
                if (pilih == "Camera"){
                    takePicture();
                    dialog.dismiss();
                } else if (pilih == "Gallery"){
                    openGallery();
                    dialog.dismiss();
                }
            }
        });

        mFoto.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showDialog(view);
            }
        });

    }

    private void openGallery() {
        try {
            Intent intent = new Intent(Intent.ACTION_GET_CONTENT);
            intent.setType("image/*");
            startActivityForResult(intent,RC_OPEN_GALLERY);
        } catch (Exception ex){
            ex.printStackTrace();
        }
    }

    private void takePicture() {
        try {
            Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
            mFileUri = getOutputMediaFileUri(MEDIA_TYPE_IMAGE);
            intent.putExtra(MediaStore.EXTRA_OUTPUT, mFileUri);
//            intent.setType("image/*"); //masalahnyo
            Log.d(TAG,"intent : " + intent);
            startActivityForResult(intent,RC_TAKE_PHOTO);
        } catch (Exception ex){
            ex.printStackTrace();
            System.out.println("GAGAAALLL : " + ex.getMessage().toString());
        }
    }

    public Uri getOutputMediaFileUri(int type) {
        return Uri.fromFile(getOutputMediaFile(type));
    }

    private static File getOutputMediaFile(int type) {

        // External sdcard location
        File mediaStorageDir = new File(
                Environment
                        .getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES),
                IMAGE_DIRECTORY_NAME);

        // Create the storage directory if it does not exist
        if (!mediaStorageDir.exists()) {
            if (!mediaStorageDir.mkdirs()) {
                Log.d(IMAGE_DIRECTORY_NAME, "Oops! Failed create "
                        + IMAGE_DIRECTORY_NAME + " directory");
                return null;
            }
        }
        // Create a media file name
        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss",
                Locale.getDefault()).format(new Date());
        File mediaFile;
        if (type == MEDIA_TYPE_IMAGE) {
            mediaFile = new File(mediaStorageDir.getPath() + File.separator
                    + "IMG_" + timeStamp + ".jpg");
        } else {
            return null;
        }

        return mediaFile;
    }

    public void showDialog(View view){
        AlertDialog.Builder builder = new AlertDialog.Builder(PostActivity.this);
        builder.setCancelable(true);
        builder.setView(listView);
        dialog = builder.create();
        dialog.show();
    }


//    @Override
//    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
//        //super.onActivityResult(requestCode, resultCode, data);
//        if (requestCode == RC_OPEN_GALLERY) {
//            if (resultCode == RESULT_OK) {
//                mFileUri = data.getData();
//                Log.d(TAG, "mFileUrl : " + mFileUri);
//                mFoto.setImageURI(mFileUri);
//                RelativeLayout.LayoutParams layoutParams = (RelativeLayout.LayoutParams) mFoto.getLayoutParams();
//                layoutParams.width = 800;
//                layoutParams.height = 300;
//                layoutParams.addRule(RelativeLayout.CENTER_IN_PARENT, 0);
//                layoutParams.addRule(RelativeLayout.CENTER_HORIZONTAL, RelativeLayout.TRUE);
//
//
//                mFoto.setLayoutParams(layoutParams);
//                mCamera.setVisibility(View.GONE);
//                tvTambahFoto.setVisibility(View.GONE);
//
//
//            } else {
//                Log.e(TAG, "Photo file is empty");
//            }
//
//
//        }
//
//    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.main,menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        int id = item.getItemId();
        caption = mEtWrite.getText().toString();
        if (!caption.isEmpty()){
            if (id == R.id.sendPost){
                savePost();
            }
        } else {
            Toast.makeText(PostActivity.this,"Tulis sesuatu!", Toast.LENGTH_LONG).show();
        }


        return super.onOptionsItemSelected(item);
    }

    private void savePost() {

        if (pilih == "Camera"){
            uploadImageCamera();
        } else if (pilih == "Gallery"){
            uploadImageGallery();
        }
        Log.d(TAG,"photoURL : " + photoUrl);
        Log.d(TAG,"Caption di savePost : " + caption);

    }

    private void ambilFullname(final String caption, final ProgressDialog pd) {
        final String userId = mAuth.getCurrentUser().getUid();
        mUser.child(userId).addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                String photoprofile;
                String sekolahId;
                sekolahId = dataSnapshot.child("sekolahId").getValue(String.class);
                mFullname = dataSnapshot.child("fullName").getValue(String.class);
                photoprofile = dataSnapshot.child("photoprofile").getValue(String.class);
                if (photoprofile == null || photoprofile == ""){
                    photoprofile = null;
                }
                Log.d(TAG,"fullName : " + mFullname);
                Log.d(TAG,"photoUrl : " + photoUrl);
                Post post = new Post(mFullname, caption, photoUrl,0,0, photoprofile);
                String push = mPost.push().getKey();
                String beranda = mSpinner.getSelectedItem().toString();
                if (beranda == "public"){
                    mPost.child(beranda).child(push).setValue(post).addOnCompleteListener(new OnCompleteListener<Void>() {
                        @Override
                        public void onComplete(@NonNull Task<Void> task) {
                            if (task.isSuccessful()){
                                Toast.makeText(PostActivity.this,"Berhasil dipost!", Toast.LENGTH_LONG).show();
                                pd.dismiss();
                                startActivity(new Intent(PostActivity.this, MainActivity.class));
                            } else {
                                Toast.makeText(PostActivity.this,"Gagal, coba lagi!", Toast.LENGTH_LONG).show();
                            }
                        }
                    });
                    ProfilePost pp = new ProfilePost(caption, 0,0, photoUrl);
                    mProfile.child(userId).child(push).setValue(pp);
                } else {
                    mPost.child(beranda).child(sekolahId).child(push).setValue(post).addOnCompleteListener(new OnCompleteListener<Void>() {
                        @Override
                        public void onComplete(@NonNull Task<Void> task) {
                            if (task.isSuccessful()){
                                Toast.makeText(PostActivity.this,"Berhasil dipost!", Toast.LENGTH_LONG).show();
                                startActivity(new Intent(PostActivity.this, MainActivity.class));
                            } else {
                                Toast.makeText(PostActivity.this,"Gagal, coba lagi!", Toast.LENGTH_LONG).show();
                            }
                        }
                    });

                    ProfilePost pp = new ProfilePost(caption, 0,0, photoUrl);
                    mProfile.child(userId).child(push).setValue(pp);
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });

    }

    private void uploadImageGallery() {
        if (mFileUri != null) {
            final ProgressDialog pd = new ProgressDialog(this);
            pd.setTitle("Uploading...");
            pd.show();

            Log.d(TAG, "file uri :"+mFileUri);
            StorageReference ref = storageReference.child("image").child(UUID.randomUUID().toString()+".jpg");
            ref.putFile(mFileUri)
                    .addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
                        @Override
                        public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {
                            Toast.makeText(getApplicationContext(), "Uploaded", Toast.LENGTH_SHORT).show();
                            Uri downloadUri = taskSnapshot.getDownloadUrl();
                            photoUrl = downloadUri.toString();
                            Log.d(TAG, "downloadUri : " + downloadUri);
                            Log.d(TAG, "photoURL : " + photoUrl);


                            if (!caption.isEmpty() && photoUrl != null){
                                ambilFullname(caption, pd);
                            } else{
                                Toast.makeText(PostActivity.this,"caption : " + caption + "\n" + "mFileUri : " + mFileUri, Toast.LENGTH_LONG).show();
                            }

                            pd.dismiss();


                        }
                    })
                    .addOnFailureListener(new OnFailureListener() {
                        @Override
                        public void onFailure(@NonNull Exception e) {
                            pd.dismiss();
                            Toast.makeText(getApplicationContext(), "Failed", Toast.LENGTH_SHORT).show();
                        }
                    })
                    .addOnProgressListener(new OnProgressListener<UploadTask.TaskSnapshot>() {
                        @Override
                        public void onProgress(UploadTask.TaskSnapshot taskSnapshot) {
                            double progress = (100.0 * taskSnapshot.getBytesTransferred() / taskSnapshot.getTotalByteCount());
                            pd.setMessage("Uploaded " + (int) progress + "%");
                        }
                    });
        } else {
            Toast.makeText(getApplicationContext(), "URL is nothing", Toast.LENGTH_SHORT).show();
        }

    }

    private void uploadImageCamera() {
        if (mFileUri != null) {
            final ProgressDialog pd = new ProgressDialog(PostActivity.this);
            pd.setTitle("Uploading...");
            pd.show();

            StorageReference ref = storageReference.child("image/" + mFileUri.getLastPathSegment());
            ref.child("image");
            file = Uri.fromFile(new File(mFileUri.getPath()));
            UploadTask uploadtask = ref.putFile(file);
            Log.d(TAG, "File fUri : " + file);
            uploadtask.addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
                @Override
                public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {
                    pd.dismiss();
                    Toast.makeText(getApplicationContext(), "Uploaded", Toast.LENGTH_SHORT).show();
                    Uri downloadUri = taskSnapshot.getDownloadUrl();
                    photoUrl = downloadUri.toString();
                    Log.d(TAG, "downloadUri : " + downloadUri);
                    Log.d(TAG, "photoURL : " + photoUrl);
                    if (!caption.isEmpty() && photoUrl != null){
                        ambilFullname(caption, pd);
                    } else{
                        Toast.makeText(PostActivity.this,"caption : " + caption + "\n" + "mFileUri : " + mFileUri, Toast.LENGTH_LONG).show();
                    }

                }
            })
                    .addOnFailureListener(new OnFailureListener() {
                        @Override
                        public void onFailure(@NonNull Exception e) {
                            pd.dismiss();
                            Toast.makeText(getApplicationContext(), "Failed", Toast.LENGTH_SHORT).show();
                        }
                    })
                    .addOnProgressListener(new OnProgressListener<UploadTask.TaskSnapshot>() {
                        @Override
                        public void onProgress(UploadTask.TaskSnapshot taskSnapshot) {
                            double progress = (100.0 * taskSnapshot.getBytesTransferred() / taskSnapshot.getTotalByteCount());
                            pd.setMessage("Uploaded " + (int) progress + "%");
                        }
                    });
        } else {
            Toast.makeText(getApplicationContext(), "URL is nothing", Toast.LENGTH_SHORT).show();

        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
//        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == RC_TAKE_PHOTO){
            if (resultCode == RESULT_OK){

                RelativeLayout.LayoutParams layoutParams = (RelativeLayout.LayoutParams) mFoto.getLayoutParams();
                layoutParams.width = 800;
                layoutParams.height = 300;
                layoutParams.addRule(RelativeLayout.CENTER_IN_PARENT, 0);
                layoutParams.addRule(RelativeLayout.CENTER_HORIZONTAL, RelativeLayout.TRUE);
                mFoto.setLayoutParams(layoutParams);

                if (mFileUri.getPath() != null) {

                    String getPath = mFileUri.getPath();
                    Log.d(TAG, "path 1 : " + getPath);
                    if (getPath.isEmpty()) {
                        getPath = mFileUri.getPath();
                        Log.d(TAG, "path 2 : " + getPath);
                    }

                    resizePhoto(getPath);
                    previewCapturedImage();
                }


                mCamera.setVisibility(View.GONE);
                tvTambahFoto.setVisibility(View.GONE);
                caption = mEtWrite.getText().toString();

                if (!runtime_permissions());


            } else {
                Log.e(TAG,"Photo file is empty");
            }
        } else if (requestCode == RC_OPEN_GALLERY){
            if (resultCode == RESULT_OK){
                mFileUri = data.getData();
                Log.d(TAG,"mFileUrl : " + mFileUri );
                mFoto.setImageURI(mFileUri);
                RelativeLayout.LayoutParams layoutParams = (RelativeLayout.LayoutParams) mFoto.getLayoutParams();
                layoutParams.width = 800;
                layoutParams.height = 300;
                layoutParams.addRule(RelativeLayout.CENTER_IN_PARENT, 0);
                layoutParams.addRule(RelativeLayout.CENTER_HORIZONTAL, RelativeLayout.TRUE);


                mCamera.setVisibility(View.GONE);
                tvTambahFoto.setVisibility(View.GONE);
                caption = mEtWrite.getText().toString();
                if (!runtime_permissions());

            } else{
                Log.e(TAG,"Photo file is empty");
            }
        }
    }

    private void previewCapturedImage() {

        // Get the dimensions of the View
        int targetW = mFoto.getWidth();
        int targetH = mFoto.getHeight();

        // Get the dimensions of the bitmap
        BitmapFactory.Options bmOptions = new BitmapFactory.Options();
        bmOptions.inJustDecodeBounds = true;
        BitmapFactory.decodeFile(mFileUri.getPath(), bmOptions);
        int photoW = bmOptions.outWidth;
        int photoH = bmOptions.outHeight;

        System.out.println("photoW: " + photoW + ", " + "photoH: " + photoH);

        // Determine how much to scale down the image
        int scaleFactor = Math.min(photoW / targetW, photoH / targetH);

        // Decode the image file into a Bitmap sized to fill the View
        bmOptions.inJustDecodeBounds = false;
        bmOptions.inSampleSize = scaleFactor;
        bmOptions.inPurgeable = true;

        Bitmap bitmap = BitmapFactory.decodeFile(mFileUri.getPath(), bmOptions);
        mFoto.setImageBitmap(bitmap);
    }

    private void resizePhoto(String uri) {
        Bitmap photo = BitmapFactory.decodeFile(uri);
        float scale = photo.getHeight() / 720f;
        int width = (int) (Math.round(photo.getWidth() / scale));

        photo = Bitmap.createScaledBitmap(photo, width, 720, false);
        ByteArrayOutputStream bytes = new ByteArrayOutputStream();
        photo.compress(Bitmap.CompressFormat.JPEG, 80, bytes);

        File mediaStorageDir = new File(
                Environment
                        .getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES),
                IMAGE_DIRECTORY_NAME);

        File resizedFile;
        resizedFile = new File(mediaStorageDir.getPath() + File.separator
                + mFileUri.getLastPathSegment());

        try {
            resizedFile.createNewFile();
            FileOutputStream fo = new FileOutputStream(resizedFile);
            fo.write(bytes.toByteArray());
            fo.close();
        } catch (IOException e) {
            e.printStackTrace();
        }


    }

    private boolean runtime_permissions() {
        if (Build.VERSION.SDK_INT >= 23 && ContextCompat.checkSelfPermission(this,
                Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED &&
                ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION)
                        != PackageManager.PERMISSION_GRANTED) {

            requestPermissions(new String[]{Manifest.permission.ACCESS_FINE_LOCATION,
                    Manifest.permission.ACCESS_COARSE_LOCATION}, 100);

//            requestPermissions(new String[]{android.Manifest.permission.ACCESS_FINE_LOCATION,
//                    android.Manifest.permission.ACCESS_COARSE_LOCATION}, 100);

            return true;
        }
        return false;
    }

    public void permission(){
        if (ContextCompat.checkSelfPermission(PostActivity.this,
                Manifest.permission.CAMERA)
                != PackageManager.PERMISSION_GRANTED) {

            // Should we show an explanation?
            if (ActivityCompat.shouldShowRequestPermissionRationale(PostActivity.this,
                    Manifest.permission.CAMERA)) {

                // Show an expanation to the user *asynchronously* -- don't block
                // this thread waiting for the user's response! After the user
                // sees the explanation, try again to request the permission.

            } else {

                // No explanation needed, we can request the permission.

                ActivityCompat.requestPermissions(PostActivity.this,
                        new String[]{Manifest.permission.CAMERA},
                        MY_PERMISSIONS_REQUEST_CAMERA);

                // MY_PERMISSIONS_REQUEST_READ_CONTACTS is an
                // app-defined int constant. The callback method gets the
                // result of the request.
            }
        } else if (ContextCompat.checkSelfPermission(PostActivity.this,
                Manifest.permission.READ_EXTERNAL_STORAGE)
                != PackageManager.PERMISSION_GRANTED) {
            // Should we show an explanation?
            if (ActivityCompat.shouldShowRequestPermissionRationale(PostActivity.this,
                    Manifest.permission.READ_EXTERNAL_STORAGE)) {

                // Show an expanation to the user *asynchronously* -- don't block
                // this thread waiting for the user's response! After the user
                // sees the explanation, try again to request the permission.

            } else {

                // No explanation needed, we can request the permission.

                ActivityCompat.requestPermissions(PostActivity.this,
                        new String[]{Manifest.permission.READ_EXTERNAL_STORAGE},
                        MY_PERMISSIONS_REQUEST_READ_EXTERNAL_STORAGE);

                // MY_PERMISSIONS_REQUEST_READ_CONTACTS is an
                // app-defined int constant. The callback method gets the
                // result of the request.
            }
        } else if (ContextCompat.checkSelfPermission(PostActivity.this,
                Manifest.permission.WRITE_EXTERNAL_STORAGE)
                != PackageManager.PERMISSION_GRANTED) {
            // Should we show an explanation?
            if (ActivityCompat.shouldShowRequestPermissionRationale(PostActivity.this,
                    Manifest.permission.WRITE_EXTERNAL_STORAGE)) {

                // Show an expanation to the user *asynchronously* -- don't block
                // this thread waiting for the user's response! After the user
                // sees the explanation, try again to request the permission.

            } else {

                // No explanation needed, we can request the permission.

                ActivityCompat.requestPermissions(PostActivity.this,
                        new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE},
                        MY_PERMISSIONS_REQUEST_WRITE_EXTERNAL_STORAGE);

                // MY_PERMISSIONS_REQUEST_READ_CONTACTS is an
                // app-defined int constant. The callback method gets the
                // result of the request.
            }
        }

    }
}
