package com.example.utomoardy.edunitas.fragment;

import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.example.utomoardy.edunitas.R;
import com.example.utomoardy.edunitas.module.Post;
import com.example.utomoardy.edunitas.viewHolder.PrivateViewHolder;
import com.firebase.ui.database.FirebaseRecyclerAdapter;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link PrivateFragment.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link PrivateFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class PrivateFragment extends Fragment {

    FirebaseAuth mAuth;
    FirebaseDatabase mDatabase;
    DatabaseReference mRootRef, mUserRef, mPostRef;
    FirebaseStorage mStorage;
    StorageReference storageReference;

    String sekolahId;

    RecyclerView mRecyclerView;
    FirebaseRecyclerAdapter<Post, PrivateViewHolder> mAdapter;
    LinearLayoutManager mManager;

    String TAG = "PrivateFragment";

    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    private OnFragmentInteractionListener mListener;

    public PrivateFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment PrivateFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static PrivateFragment newInstance(String param1, String param2) {
        PrivateFragment fragment = new PrivateFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        mStorage = FirebaseStorage.getInstance();
        storageReference = mStorage.getReference();
        mAuth = FirebaseAuth.getInstance();
        mDatabase = FirebaseDatabase.getInstance();
        mRootRef = mDatabase.getReference();
        mUserRef = mRootRef.child("users");
        mPostRef = mRootRef.child("posts");

        View view = inflater.inflate(R.layout.fragment_public, container, false);

        mRecyclerView = view.findViewById(R.id.public_recyclerview);
        mManager = new LinearLayoutManager(this.getContext());

        mManager.setReverseLayout(true);
        mManager.setStackFromEnd(true);

        mPostRef.child("private").addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                if (dataSnapshot.getChildren() != null) {
                    Log.d("PublicFragment", "datasnapshot :" + dataSnapshot.getValue());
                    mRecyclerView.setLayoutManager(mManager);
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });

        // Inflate the layout for this fragment
        return view;
    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
//            throw new RuntimeException(context.toString()
//                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }

    @Override
    public void onResume() {
        super.onResume();

        if (mDatabase == null) {
            mDatabase = FirebaseDatabase.getInstance();
        }
        mRootRef = mDatabase.getReference();
        mUserRef = mRootRef.child("users");
        mPostRef = mRootRef.child("posts");

        mAuth = FirebaseAuth.getInstance();

        Log.d("Private F" , "UID : " + mAuth.getCurrentUser().getUid());
        String uid = mAuth.getCurrentUser().getUid();
        mUserRef.child(uid).addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                sekolahId = dataSnapshot.child("sekolahId").getValue(String.class);
                Log.d("Private F", "sekolah Id datasnapshot : " + sekolahId);
                mPostRef.child("private").addValueEventListener(new ValueEventListener() {
                    @Override
                    public void onDataChange(DataSnapshot dataSnapshot) {
                        Log.d(TAG, "dataSnapshot :"+dataSnapshot.getValue());
                        if (dataSnapshot.getValue() == null) {
                            Toast.makeText(getContext(), "Belum ada content", Toast.LENGTH_SHORT).show();
                        } else {

                            mAdapter = new FirebaseRecyclerAdapter<Post, PrivateViewHolder>(
                                    Post.class,
                                    R.layout.item_public,
                                    PrivateViewHolder.class,
                                    mPostRef.child("private").child(sekolahId)
                            ) {
                                @Override
                                protected void populateViewHolder(PrivateViewHolder viewHolder, Post model, int position) {
                                    final DatabaseReference postRef = getRef(position);
                                    final String postKey = postRef.getKey();

                                    viewHolder.bindToPrivate(model, postKey);
                                    Log.d("PrivateFragment", "Postkey : " + postKey);
                                }
                            };

                            mAdapter.registerAdapterDataObserver(new RecyclerView.AdapterDataObserver() {
                                @Override
                                public void onItemRangeInserted(int positionStart, int itemCount) {
                                    super.onItemRangeInserted(positionStart, itemCount);
                                    mRecyclerView.smoothScrollToPosition(mAdapter.getItemCount());
                                }
                            });

                            mPostRef.child("private").addListenerForSingleValueEvent(new ValueEventListener() {
                                @Override
                                public void onDataChange(DataSnapshot dataSnapshot) {
                                    if (dataSnapshot.getChildren() != null) {
                                        Log.d("PrivateFragment", "datasnapshot resume :" + dataSnapshot.getValue());
                                        mRecyclerView.setLayoutManager(mManager);
                                        mRecyclerView.setAdapter(mAdapter);
                                        mRecyclerView.getRecycledViewPool().clear();
                                        mAdapter.notifyDataSetChanged();
                                    }
                                }

                                @Override
                                public void onCancelled(DatabaseError databaseError) {

                                }
                            });
                        }
                    }

                    @Override
                    public void onCancelled(DatabaseError databaseError) {

                    }
                });

            }
            @Override
            public void onCancelled(DatabaseError databaseError) {

                Log.d("Private F" , "gagal");
            }
        });


        Log.d("Private F", "sekolahID : " + sekolahId);


    }

}
