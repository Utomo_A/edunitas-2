package com.example.utomoardy.edunitas.activity;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.example.utomoardy.edunitas.R;
import com.example.utomoardy.edunitas.module.Prestasi;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;

public class SettingActivity extends AppCompatActivity {
    EditText etJuara, etKeterangan;
    Button btnTambahPrestasi;

    FirebaseAuth mAuth;
    FirebaseDatabase mDatabase;
    DatabaseReference mRootRef, mUserRef, mPostRef, mPrestasiRef;
    FirebaseStorage mStorage;
    StorageReference storageReference;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_setting);

        mStorage = FirebaseStorage.getInstance();
        storageReference = mStorage.getReference();
        mAuth = FirebaseAuth.getInstance();
        mDatabase = FirebaseDatabase.getInstance();
        mRootRef = mDatabase.getReference();
        mUserRef = mRootRef.child("users");
        mPostRef = mRootRef.child("posts");
        mPrestasiRef = mRootRef.child("prestasi");

        etJuara = findViewById(R.id.etJuara);
        etKeterangan = findViewById(R.id.etKeterangan);
        btnTambahPrestasi = findViewById(R.id.btnTambahPrestasi);

        btnTambahPrestasi.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String juara = etJuara.getText().toString();
                String ket = etKeterangan.getText().toString();
                String uid = mAuth.getCurrentUser().getUid();

                Prestasi prestasi = new Prestasi(juara,ket);

                mPrestasiRef.child(uid).push().setValue(prestasi).addOnCompleteListener(new OnCompleteListener<Void>() {
                    @Override
                    public void onComplete(@NonNull Task<Void> task) {
                        if (task.isSuccessful()){
                            etJuara.setText("");
                            etKeterangan.setText("");
                        }
                    }
                });

            }
        });

    }
}
