package com.example.utomoardy.edunitas.viewHolder;

import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.utomoardy.edunitas.R;
import com.example.utomoardy.edunitas.activity.ViewProfileActivity;
import com.example.utomoardy.edunitas.module.School;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.squareup.picasso.Picasso;

/**
 * Created by Utomo Ardy on 23/03/2018.
 */

public class NearbySchoolViewHolder extends RecyclerView.ViewHolder {
    TextView tvSchoolName;
    ImageView ivSchool;

    FirebaseAuth mAuth;
    DatabaseReference mRootRef, mSchool;

    School displayedMessage = new School();

    public String postKey;
    String TAG = "NearbySchoolViewHolder";

    public NearbySchoolViewHolder(final View itemView) {
        super(itemView);

        mAuth = FirebaseAuth.getInstance();
        mRootRef = FirebaseDatabase.getInstance().getReference();
        mSchool = mRootRef.child("schools");

        tvSchoolName = itemView.findViewById(R.id.tv_school_name);
        ivSchool = itemView.findViewById(R.id.iv_school);
        ivSchool.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(itemView.getContext(),ViewProfileActivity.class);
                intent.putExtra("post_key", postKey);
                Log.d(TAG, "postKey :"+postKey);
                itemView.getContext().startActivity(intent);
            }
        });
    }

    public void bindToPost(final School school, String postKey){
        this.postKey = postKey;
        Log.d(TAG, "postKey 2 :"+postKey);

        displayedMessage = school;

        String userId = mAuth.getCurrentUser().getUid();
        mSchool.child(userId).addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                String name = dataSnapshot.child("schoolName").getValue().toString();
                String imgUrl = dataSnapshot.child("schoolImageUrl").getValue().toString();
                Log.d(TAG, "schoolName :"+name);
                Log.d(TAG, "imgUrl :"+ imgUrl);

                tvSchoolName.setText(name);
                if (imgUrl.equals("")){
                    Log.d(TAG, "foto belum ditambahkan");
                }else {
                    Picasso.with(itemView.getContext()).load(imgUrl).into(ivSchool);
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
    }
}
