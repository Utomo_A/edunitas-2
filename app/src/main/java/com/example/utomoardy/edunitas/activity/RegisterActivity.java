package com.example.utomoardy.edunitas.activity;

import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.example.utomoardy.edunitas.R;
import com.example.utomoardy.edunitas.module.User;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

public class RegisterActivity extends AppCompatActivity {
    FirebaseAuth mAuth;
    DatabaseReference mRootRef, mUser;

    Button btnRegister;
    EditText etEmail, etFullname, etPassword;
    ProgressBar mProgressBar;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);

        mAuth = FirebaseAuth.getInstance();
        mRootRef = FirebaseDatabase.getInstance().getReference();
        mUser = mRootRef.child("users");

        etEmail = findViewById(R.id.et_email);
        etFullname = findViewById(R.id.et_fullname);
        etPassword = findViewById(R.id.et_password);

        mProgressBar = findViewById(R.id.progress_bar);

        btnRegister = findViewById(R.id.btn_register);
        btnRegister.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                final String email = etEmail.getText().toString();
                final String password = etPassword.getText().toString();
                final String fullname = etFullname.getText().toString();

                if (TextUtils.isEmpty(fullname)){
                    etFullname.setError("Nama lengkap harus diisi");
                }else if (TextUtils.isEmpty(email)){
                    etEmail.setError("Email harus diisi");
                }else if (TextUtils.isEmpty(password)){
                    etPassword.setError("Kata sandi harus diisi");
                }else {
                    mProgressBar.setVisibility(View.VISIBLE);
                    mAuth.createUserWithEmailAndPassword(email, password).addOnCompleteListener(new OnCompleteListener<AuthResult>() {
                        @Override
                        public void onComplete(@NonNull Task<AuthResult> task) {
                            if (task.isSuccessful()){
                                writeUser(email, password, fullname);
                            }else {
                                mProgressBar.setVisibility(View.INVISIBLE);
                                Toast.makeText(RegisterActivity.this, task.getException().getLocalizedMessage(), Toast.LENGTH_LONG).show();
                            }
                        }
                    });
                }
            }
        });
    }

    private void writeUser(String email, String password, final String fullname) {
        String statusUser = "umum";
        String aktif = "yes";
        String verifikasi = "no";
        String photoProfile = "";
        String schoolId ="";
        String schoolName = "";

        String uid = mAuth.getCurrentUser().getUid();
        User user = new User(email, password, fullname, statusUser, aktif, verifikasi, photoProfile, schoolId, schoolName, uid);
        mUser.child(uid).setValue(user).addOnCompleteListener(new OnCompleteListener<Void>() {
            @Override
            public void onComplete(@NonNull Task<Void> task) {
                if (task.isSuccessful()){
                    mProgressBar.setVisibility(View.INVISIBLE);
                    Toast.makeText(RegisterActivity.this, "Akun "+fullname+" Berhasil dibuat", Toast.LENGTH_SHORT).show();
                    Intent intent = new Intent(RegisterActivity.this, MainActivity.class);
                    intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                    startActivity(intent);
                    finish();
                }else {
                    Toast.makeText(RegisterActivity.this, task.getException().getLocalizedMessage(), Toast.LENGTH_LONG).show();
                    mProgressBar.setVisibility(View.INVISIBLE);
                }
            }
        });
    }
}
