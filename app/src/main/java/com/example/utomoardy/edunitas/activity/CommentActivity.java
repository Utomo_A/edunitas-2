package com.example.utomoardy.edunitas.activity;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.utomoardy.edunitas.R;
import com.example.utomoardy.edunitas.Utility;
import com.example.utomoardy.edunitas.module.Comment;
import com.example.utomoardy.edunitas.viewHolder.CommentViewHolder;
import com.firebase.ui.database.FirebaseRecyclerAdapter;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.squareup.picasso.Picasso;

import de.hdodenhof.circleimageview.CircleImageView;

public class CommentActivity extends AppCompatActivity {

    private static final String TAG = "CommentActivity" ;
    String postKey, status, sekolahId;

    CircleImageView circleComment;
    TextView tvNamaPost, tvWaktuPost, tvContentPost,
    tvJumlahComment, tvNamaComment, tvWaktuComment, tvIsiComment;
    ImageView ivPhotoPost;
    EditText etIsiComment;
    Button btnComment;

    private int jumlahComment;

    FirebaseAuth mAuth;
    FirebaseDatabase mDatabase;
    DatabaseReference mRootRef, mUserRef, mPostRef, mCommentRef;
    FirebaseStorage mStorage;
    StorageReference storageReference;

    RecyclerView mRecyclerView;
    FirebaseRecyclerAdapter<Comment, CommentViewHolder> mAdapter;
    LinearLayoutManager mManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_comment);

        mStorage = FirebaseStorage.getInstance();
        storageReference = mStorage.getReference();
        mAuth = FirebaseAuth.getInstance();
        mDatabase = FirebaseDatabase.getInstance();
        mRootRef = mDatabase.getReference();
        mUserRef = mRootRef.child("users");
        mPostRef = mRootRef.child("posts");
        mCommentRef = mRootRef.child("comments");

        mRecyclerView = findViewById(R.id.public_recyclerviewComment);
        mManager = new LinearLayoutManager(this.getApplicationContext());

//        mManager.setReverseLayout(true);
//        mManager.setStackFromEnd(true);

        circleComment = findViewById(R.id.profileCommentPublic);
        tvNamaPost = findViewById(R.id.tvNamaPostComment);
        tvWaktuPost = findViewById(R.id.tvWaktuPostComment);
        tvContentPost = findViewById(R.id.tvContentPostComment);
        tvJumlahComment = findViewById(R.id.tvJumlahPostComment);
        tvNamaComment = findViewById(R.id.tvNamaYangComment);
        tvWaktuComment = findViewById(R.id.tvWaktuYangComment);
        tvIsiComment = findViewById(R.id.tvIsiComment);
        ivPhotoPost = findViewById(R.id.photoPostComment);
        etIsiComment = findViewById(R.id.etIsiComment);
        btnComment = findViewById(R.id.btnComment);

        Bundle extras = getIntent().getExtras();
        postKey = extras.getString("postKey");
        status = extras.getString("status");
        Log.d(TAG, "status:" + status);
        status = status.trim();

        if (status.equals("public")){
            Log.d(TAG,"masukpublic");
            mPostRef.child(status).child(postKey).addValueEventListener(new ValueEventListener() {
                @Override
                public void onDataChange(DataSnapshot dataSnapshot) {
                    String fullname = dataSnapshot.child("fullname").getValue(String.class);
                    String isiPost = dataSnapshot.child("content").getValue(String.class);
                    String photo = dataSnapshot.child("photo").getValue(String.class);
                    String photoprofile = dataSnapshot.child("photoprofile").getValue(String.class);
                    Log.d(TAG, "fullname : " + fullname + " isiPost : " + isiPost + " photo : " + photo);
                    jumlahComment = dataSnapshot.child("sumComment").getValue(int.class);
                    Object timestamp = dataSnapshot.child("timestamp").getValue(Object.class);

                    tvNamaPost.setText(fullname);
                    tvContentPost.setText(isiPost);
                    Log.d(TAG, "photoprofile:" + photoprofile);
                    Picasso.with(getApplicationContext()).load(photo).into(ivPhotoPost);
                    if (photoprofile.isEmpty()){
                        Picasso.with(getApplicationContext()).load("https://firebasestorage.googleapis.com/v0/b/webkppp.appspot.com/o/image%2Fperson.png?alt=media&token=45155dbd-a96c-434a-b85e-1f5dde9f16aa").into(circleComment);
                    } else {
                        Picasso.with(getApplicationContext()).load(photoprofile).into(circleComment);
                    }


                    Utility util = new Utility();
                    String postTime = util.getPostTime((Long) timestamp);
                    tvWaktuPost.setText(postTime);
                    tvJumlahComment.setText(String.valueOf(jumlahComment));
                }

                @Override
                public void onCancelled(DatabaseError databaseError) {

                }
            });
        } else if (status.equals("private")){
            Log.d(TAG,"masukprivate");
            String userId = mAuth.getCurrentUser().getUid();
            mUserRef.child(userId).addValueEventListener(new ValueEventListener() {
                @Override
                public void onDataChange(DataSnapshot dataSnapshot) {
                    sekolahId = dataSnapshot.child("sekolahId").getValue(String.class);
                    mPostRef.child(status).child(sekolahId).child(postKey).addValueEventListener(new ValueEventListener() {
                        @Override
                        public void onDataChange(DataSnapshot dataSnapshot) {
                            String fullname = dataSnapshot.child("fullname").getValue(String.class);
                            String isiPost = dataSnapshot.child("content").getValue(String.class);
                            String photo = dataSnapshot.child("photo").getValue(String.class);
                            String photoprofile = dataSnapshot.child("photoprofile").getValue(String.class);
                            jumlahComment = dataSnapshot.child("sumComment").getValue(int.class);
                            Object timestamp = dataSnapshot.child("timestamp").getValue(Object.class);
                            Log.d(TAG,"fullname+isi+photo : " + fullname + isiPost + photo);

                            tvNamaPost.setText(fullname);
                            tvContentPost.setText(isiPost);
                            Picasso.with(getApplicationContext()).load(photo).into(ivPhotoPost);
                            Picasso.with(getApplicationContext()).load(photoprofile).into(circleComment);

                            Utility util = new Utility();
                            String postTime = util.getPostTime((Long) timestamp);
                            tvWaktuPost.setText(postTime);
                            tvJumlahComment.setText(String.valueOf(jumlahComment));
                        }

                        @Override
                        public void onCancelled(DatabaseError databaseError) {

                        }
                    });

                }

                @Override
                public void onCancelled(DatabaseError databaseError) {

                }
            });

         } else {
            Log.d(TAG,"masukelse");
        }



        btnComment.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                saveComment();
            }
        });

        mCommentRef.child(postKey).addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                if (dataSnapshot.getChildren() != null) {
                    Log.d("Comment Act", "datasnapshot :" + dataSnapshot.getValue());
                    mRecyclerView.setLayoutManager(mManager);
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });

    }

    private void saveComment() {
        String uid = mAuth.getCurrentUser().getUid();
        mUserRef.child(uid).addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                String isiKomen = etIsiComment.getText().toString();
                String fullnameComment = dataSnapshot.child("fullName").getValue(String.class);
                Comment comment = new Comment(isiKomen, fullnameComment);
                mCommentRef.child(postKey).push().setValue(comment);
                jumlahComment = jumlahComment+1;
                mPostRef.child(status).child(postKey).child("sumComment").setValue(jumlahComment);
                tvJumlahComment.setText(String.valueOf(jumlahComment));
                etIsiComment.setText("");
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
    }

    @Override
    public void onResume() {
        super.onResume();

        if (mDatabase == null) {
            mDatabase = FirebaseDatabase.getInstance();
        }
        mRootRef = mDatabase.getReference();
        mUserRef = mRootRef.child("users");
        mPostRef = mRootRef.child("posts");
        mCommentRef = mRootRef.child("comments");

        mAuth = FirebaseAuth.getInstance();

        mAdapter = new FirebaseRecyclerAdapter<Comment, CommentViewHolder>(
                Comment.class,
                R.layout.item_comment,
                CommentViewHolder.class,
                mCommentRef.child(postKey)
        ) {
            @Override
            protected void populateViewHolder(CommentViewHolder viewHolder, Comment model, int position) {
                final DatabaseReference postRef = getRef(position);
                final String postKey = postRef.getKey();

                viewHolder.bindToComment(model, postKey);
                Log.d("PublicFragment","Postkey : " + postKey);
            }
        };

        mAdapter.registerAdapterDataObserver(new RecyclerView.AdapterDataObserver() {
            @Override
            public void onItemRangeInserted(int positionStart, int itemCount) {
                super.onItemRangeInserted(positionStart, itemCount);
                mRecyclerView.smoothScrollToPosition(mAdapter.getItemCount());
            }
        });

        mCommentRef.child(status).addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                if (dataSnapshot.getChildren() != null) {
                    Log.d("PublicFragment", "datasnapshot resume :" + dataSnapshot.getValue());
                    mRecyclerView.setLayoutManager(mManager);
                    mRecyclerView.setAdapter(mAdapter);
                    mRecyclerView.getRecycledViewPool().clear();
                    mAdapter.notifyDataSetChanged();
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });

    }

}
