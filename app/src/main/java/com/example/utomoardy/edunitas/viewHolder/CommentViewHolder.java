package com.example.utomoardy.edunitas.viewHolder;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

import com.example.utomoardy.edunitas.R;
import com.example.utomoardy.edunitas.Utility;
import com.example.utomoardy.edunitas.module.Comment;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;

/**
 * Created by windows on 3/15/2018.
 */

public class CommentViewHolder extends RecyclerView.ViewHolder {

    FirebaseAuth mAuth;
    FirebaseDatabase mDatabase;
    DatabaseReference mRootRef, mUserRef, mPostRef, mCommentRef;
    FirebaseStorage mStorage;
    StorageReference storageReference;

    TextView tvNamaComment, tvWaktuComment, tvIsiComment;

    String TAG = "CommentViewHolder";

    Comment displayComment = new Comment();
    public String commentKey;

    public CommentViewHolder(View itemView) {
        super(itemView);

        mStorage = FirebaseStorage.getInstance();
        storageReference = mStorage.getReference();
        mAuth = FirebaseAuth.getInstance();
        mDatabase = FirebaseDatabase.getInstance();
        mRootRef = mDatabase.getReference();
        mUserRef = mRootRef.child("users");
        mPostRef = mRootRef.child("posts");
        mCommentRef = mRootRef.child("comments");

        tvNamaComment = itemView.findViewById(R.id.tvNamaYangComment);
        tvWaktuComment = itemView.findViewById(R.id.tvWaktuYangComment);
        tvIsiComment = itemView.findViewById(R.id.tvIsiComment);

    }

    public void bindToComment(final Comment comment, final String commentKey) {
        this.commentKey = commentKey;
        displayComment = comment;

        tvNamaComment.setText(comment.fullname);
        tvIsiComment.setText(comment.isiComment);

        Utility util = new Utility();
        String commentTime = util.getPostTime((Long) comment.timestamp);
        tvWaktuComment.setText(commentTime);
    }

}
