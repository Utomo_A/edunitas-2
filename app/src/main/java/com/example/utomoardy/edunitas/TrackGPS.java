package com.example.utomoardy.edunitas;

import android.app.AlertDialog;
import android.app.Service;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.os.IBinder;
import android.provider.Settings;
import android.support.annotation.Nullable;
import android.util.Log;
import android.widget.Toast;

/**
 * Created by Utomo Ardy on 14/03/2018.
 */

public class TrackGPS extends Service implements LocationListener {
    private final Context mContext;


    boolean checkGPS = false;


    boolean checkNetwork = false;

    boolean canGetLocation = false;

    Location loc;
    double latitude;
    double longitude;


    private static final long MIN_DISTANCE_CHANGE_FOR_UPDATES = 10;


    private static final long MIN_TIME_BW_UPDATES = 1000 * 60 * 1;
    protected LocationManager locationManager;

    public TrackGPS(Context mContext){
        this.mContext = mContext;
        getLocation();
    }

    public Location getLocation() {
        try {
            locationManager = (LocationManager) mContext.getSystemService(LOCATION_SERVICE);
            checkGPS = locationManager.isProviderEnabled(locationManager.GPS_PROVIDER);
            checkNetwork = locationManager.isProviderEnabled(locationManager.NETWORK_PROVIDER);
            if (!checkGPS && !checkNetwork){
                Toast.makeText(mContext, "Tidak ada layanan", Toast.LENGTH_SHORT).show();
            }else {
                this.canGetLocation = true;
                if (checkNetwork){
                    Toast.makeText(mContext, "Network", Toast.LENGTH_SHORT).show();
                    try {
                        locationManager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, MIN_TIME_BW_UPDATES, MIN_DISTANCE_CHANGE_FOR_UPDATES,this);
                        Log.d("Network", "Network");
                        if (locationManager != null){
                            loc = locationManager.getLastKnownLocation(LocationManager.NETWORK_PROVIDER);
                        }
                        if (loc != null){
                            latitude = loc.getLatitude();
                            longitude = loc.getLongitude();
                        }
                    }catch (SecurityException e){

                    }
                }
            }
            if (checkGPS){
                Toast.makeText(mContext, "GPS", Toast.LENGTH_SHORT).show();
                if (loc == null){
                    try {
                        locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, MIN_TIME_BW_UPDATES, MIN_DISTANCE_CHANGE_FOR_UPDATES,this);
                        Log.d("GPS Diaktifkan", "GPS Diaktifkan");
                        if (locationManager != null){
                            loc = locationManager.getLastKnownLocation(LocationManager.GPS_PROVIDER);
                            if (loc != null){
                                latitude = loc.getLatitude();
                                longitude = loc.getLongitude();
                            }
                        }
                    }catch (SecurityException e){

                    }
                }
            }
        }catch (Exception e){
            e.printStackTrace();
        }
        return loc;
    }
    public double getLongitude(){
        if (loc != null){
            longitude = loc.getLongitude();
        }
        return longitude;
    }
    public double getLatitude(){
        if (loc != null){
            latitude = loc.getLatitude();
        }
        return latitude;
    }
    public boolean canGetLocation(){
        return this.canGetLocation;
    }
    public void showSettingAlert(){
        AlertDialog.Builder alertDialog = new AlertDialog.Builder(mContext);
        alertDialog.setTitle("GPS Tidak Diaktifkan");
        alertDialog.setMessage("Apakah Anda ingin mengaktifkan GPS");

        alertDialog.setPositiveButton("Ya", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int which) {
                Intent intent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                mContext.startActivity(intent);
            }
        });
        alertDialog.setNegativeButton("Tidak", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int which) {
                dialogInterface.cancel();
            }
        });
        alertDialog.show();
    }
    public void stopUsingGPS(){
        if (locationManager != null){
            locationManager.removeUpdates(TrackGPS.this);
        }
    }

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public void onLocationChanged(Location location) {

    }

    @Override
    public void onStatusChanged(String s, int i, Bundle bundle) {

    }

    @Override
    public void onProviderEnabled(String s) {

    }

    @Override
    public void onProviderDisabled(String s) {

    }
}
