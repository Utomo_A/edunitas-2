package com.example.utomoardy.edunitas.activity;

import android.location.Location;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.example.utomoardy.edunitas.viewHolder.NearbySchoolViewHolder;
import com.example.utomoardy.edunitas.R;
import com.example.utomoardy.edunitas.TrackGPS;
import com.example.utomoardy.edunitas.module.School;
import com.example.utomoardy.edunitas.viewHolder.SearchViewHolder;
import com.firebase.ui.database.FirebaseRecyclerAdapter;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.model.LatLng;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;

public class SearchActivity extends AppCompatActivity {

    GoogleMap mMap;
    FirebaseAuth mAuth;
    DatabaseReference mRootRef, mSchool;

    RecyclerView mRecyclerViewNearby, mRecyclerViewSearch;
    FirebaseRecyclerAdapter<School, NearbySchoolViewHolder> mAdapter;
    FirebaseRecyclerAdapter<School, SearchViewHolder> mAdapterSearch;
    LinearLayoutManager mManager, mManagerSerach;
    RecyclerView.LayoutManager mLayoutManager, mLayoutManagerSerach;

    TextView mTvNoShool, mTvSearchEmpty;
    Button mBtnSearchSchool;
    EditText mEtSearchSchool;

    String TAG = "SearchActivity";
    String schoolName;

    TrackGPS gps;
    double currentLatitude, currentLongitude;
    Location loc;

    School school = new School();

    private GoogleMap mGoogleMap;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_search);

        mAuth = FirebaseAuth.getInstance();
        mRootRef = FirebaseDatabase.getInstance().getReference();
        mSchool = mRootRef.child("schools");

        gps = new TrackGPS(SearchActivity.this);

        mEtSearchSchool = findViewById(R.id.et_search);
        mBtnSearchSchool = findViewById(R.id.btn_search);

        mTvNoShool = findViewById(R.id.tv_no_school);
        mTvSearchEmpty = findViewById(R.id.tv_search_empty);

        currentLatitude = gps.getLatitude();
        currentLongitude = gps.getLongitude();
        Log.d(TAG, "currentLatitude :" + gps.getLatitude());

        mRecyclerViewSearch = findViewById(R.id.recycler_view_search_school);
        mRecyclerViewSearch.setHasFixedSize(true);
        mManagerSerach = new LinearLayoutManager(this);
        mManagerSerach.setReverseLayout(true);
        mManagerSerach.setStackFromEnd(true);
        mRecyclerViewSearch.setLayoutManager(mManagerSerach);

        mRecyclerViewNearby = findViewById(R.id.recycler_view_nearby_school);
        mRecyclerViewNearby.setHasFixedSize(true);
        mManager = new LinearLayoutManager(this.getApplicationContext(),LinearLayoutManager.HORIZONTAL,true);
        mManager.setReverseLayout(true);
        mManager.setStackFromEnd(true);
        mRecyclerViewNearby.setLayoutManager(mManager);

        mBtnSearchSchool.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                final String searchSchoolName = mEtSearchSchool.getText().toString();
                mSchool.orderByChild("schoolName").equalTo(searchSchoolName).addValueEventListener(new ValueEventListener() {
                    @Override
                    public void onDataChange(DataSnapshot dataSnapshot) {
                        if (dataSnapshot.getValue()==null){
                            mTvSearchEmpty.setText(View.INVISIBLE);
                        }
                    }

                    @Override
                    public void onCancelled(DatabaseError databaseError) {

                    }
                });
                //**********************************************************************************
                if (TextUtils.isEmpty(searchSchoolName)){
                    mEtSearchSchool.setError("Masukan nama sekolah");
                }else {
                    mAdapterSearch = new FirebaseRecyclerAdapter<School, SearchViewHolder>(
                            School.class,
                            R.layout.item_search_school,
                            SearchViewHolder.class,
                            mSchool
                    ) {
                        @Override
                        protected void populateViewHolder(SearchViewHolder viewHolder, School model, int position) {
                            final DatabaseReference posRef = getRef(position);
                            final String postKey = posRef.getKey();
                            viewHolder.bindToPost(model, postKey, searchSchoolName);
                        }
                    };
                    mAdapterSearch.registerAdapterDataObserver(new RecyclerView.AdapterDataObserver() {
                        @Override
                        public void onItemRangeInserted(int positionStart, int itemCount) {
                            super.onItemRangeInserted(positionStart, itemCount);

                            mRecyclerViewSearch.smoothScrollToPosition(mAdapter.getItemCount());
                        }
                    });
                    mRecyclerViewSearch.setAdapter(mAdapterSearch);
                }
            }
        });

//        DatabaseReference mUser = FirebaseDatabase.getInstance().getReference().child("users");
//        mUser.orderByChild("email").equalTo("utomo@gmail.com").addValueEventListener(new ValueEventListener() {
//            @Override
//            public void onDataChange(DataSnapshot dataSnapshot) {
//                Log.d(TAG, "dataSnapshot child :"+dataSnapshot);
//            }
//
//            @Override
//            public void onCancelled(DatabaseError databaseError) {
//
//            }
//        });

        final List<School> locationData = new ArrayList<>();
        mSchool.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                    if (dataSnapshot.getValue() != null) {
                    Iterator<DataSnapshot> locationSnapshot = dataSnapshot.getChildren().iterator();
                    do {
                        HashMap<Double, Double> locationMap = new HashMap<>();
                        String schoolId = locationSnapshot.next().getKey();
                        school = dataSnapshot.child(schoolId).getValue(School.class);
                        double longitude = school.getLongitude();
                        double latitude = school.getLatitude();
                        schoolName = school.getSchoolName();
                        Log.d(TAG, "latitude :" + latitude);
                        Log.d(TAG, "longitude :" + longitude);
                        Log.d(TAG, "schoolName :" + schoolName);
                        LatLng tower = new LatLng(latitude, longitude);
                        locationData.add(school);
                        getDistanceFromLatLonInKm(currentLatitude, currentLongitude, latitude, longitude);
//                        sendRequest(loc, latitude, longitude);
                    } while (locationSnapshot.hasNext());
                } else {
                    Toast.makeText(getApplicationContext(), "Belum ada sekolah yang ditambahkan !!!", Toast.LENGTH_SHORT).show();
                    mTvNoShool.setVisibility(View.VISIBLE);
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });

    }

    public double getDistanceFromLatLonInKm(double lat1,double lon1,double lat2,double lon2) {
        int R = 6371; // Radius of the earth in km
        double dLat = deg2rad(lat2-lat1);  // deg2rad below
        double dLon = deg2rad(lon2-lon1);
        double a =
                Math.sin(dLat/2) * Math.sin(dLat/2) +
                        Math.cos(deg2rad(lat1)) * Math.cos(deg2rad(lat2)) *
                                Math.sin(dLon/2) * Math.sin(dLon/2)
                ;
        double c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1-a));
        double d = R * c; // Distance in km
        Log.d(TAG, "distance :"+d);
        setView(d);
        return d;
    }

    public double deg2rad(double deg) {
        return deg * (Math.PI/180);
    }

    private void setView(double d) {
        if (d <= 3/*3 km*/) {
            Log.d(TAG, "sekolah berhasil ditemukan");
            mAdapter = new FirebaseRecyclerAdapter<School, NearbySchoolViewHolder>(
                    School.class,
                    R.layout.item_nearby_school,
                    NearbySchoolViewHolder.class,
                    mSchool
            ) {
                @Override
                protected void populateViewHolder(NearbySchoolViewHolder viewHolder, School model, int position) {
                    final DatabaseReference posRef = getRef(position);
                    final String postKey = posRef.getKey();
                    viewHolder.bindToPost(model, postKey);
                }
            };
            mAdapter.registerAdapterDataObserver(new RecyclerView.AdapterDataObserver() {
                @Override
                public void onItemRangeInserted(int positionStart, int itemCount) {
                    super.onItemRangeInserted(positionStart, itemCount);

                    mRecyclerViewNearby.smoothScrollToPosition(mAdapter.getItemCount());
                }
            });
            mRecyclerViewNearby.setAdapter(mAdapter);
        } else {
            Log.d(TAG, "tidak ada sekolah dalam radius 3 km");
        }
    }

//    private void sendRequest(Location loc, double latSchool, double lngSchool) {
//        Log.d(TAG, "latAwal :" + gps.getLatitude());
//        String origin = gps.getLatitude() + "," + gps.getLongitude();
//        String destination = latSchool + "," + lngSchool;
//        try {
//            new DirectionFinder(this, origin, destination).execute();
//        } catch (UnsupportedEncodingException e) {
//            e.printStackTrace();
//        }
//    }
//
//    @Override
//    public void onDirectionFinderStart() {
//
//    }
//
//    @Override
//    public void onDirectionFinderSuccess(List<Route> routes) {
//        for (Route route : routes) {
//            int distance = (route.distance.value);
//            Log.d(TAG, "distance :" + distance);
//            if (distance <= 3000/*3 km*/) {
//                Log.d(TAG, "sekolah berhasil ditemukan");
//                mAdapter = new FirebaseRecyclerAdapter<School, NearbySchoolViewHolder>(
//                        School.class,
//                        R.layout.item_nearby_school,
//                        NearbySchoolViewHolder.class,
//                        mSchool
//                ) {
//                    @Override
//                    protected void populateViewHolder(NearbySchoolViewHolder viewHolder, School model, int position) {
//                        final DatabaseReference posRef = getRef(position);
//                        final String postKey = posRef.getKey();
//                        viewHolder.bindToPost(model, postKey);
//                    }
//                };
//                mAdapter.registerAdapterDataObserver(new RecyclerView.AdapterDataObserver() {
//                    @Override
//                    public void onItemRangeInserted(int positionStart, int itemCount) {
//                        super.onItemRangeInserted(positionStart, itemCount);
//
//                        mRecyclerViewNearby.smoothScrollToPosition(mAdapter.getItemCount());
//                    }
//                });
//                mRecyclerViewNearby.setAdapter(mAdapter);
//            } else {
//                Log.d(TAG, "tidak ada sekolah dalam radius 3 km");
//            }
//        }
//    }
}
