package com.example.utomoardy.edunitas.activity;

import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.example.utomoardy.edunitas.R;
import com.example.utomoardy.edunitas.module.Achivement;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

public class AddAchivementActivity extends AppCompatActivity {

    EditText mEtAchivement, mEtDescription;

    FirebaseAuth mAuth;
    DatabaseReference mRootRef, mAchivement, mUser, mSchool;

    ProgressBar mProgresBar;

    String TAG = "AddAchivementActivity";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_achivement);

        mAuth = FirebaseAuth.getInstance();
        mRootRef = FirebaseDatabase.getInstance().getReference();
        mAchivement = mRootRef.child("achivements");
        mUser = mRootRef.child("users");
        mSchool = mRootRef.child("schools");

        mEtAchivement = findViewById(R.id.et_title_achivement);
        mEtDescription = findViewById(R.id.et_description_achivement);

        mProgresBar = findViewById(R.id.progress_bar);

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {

        getMenuInflater().inflate(R.menu.setting_menu,menu);

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        int id = item.getItemId();

        if (id == R.id.item_done){
            String achivement_title = mEtAchivement.getText().toString();
            String achivement_description = mEtDescription.getText().toString();
            String achivementId = mAchivement.push().getKey();
            String userId = mAuth.getCurrentUser().getUid();

            mProgresBar.setVisibility(View.VISIBLE);

            Achivement achivement = new Achivement(achivement_title, achivement_description);

            mAchivement.child(userId).child(achivementId).setValue(achivement).addOnCompleteListener(new OnCompleteListener<Void>() {
                @Override
                public void onComplete(@NonNull Task<Void> task) {
                    if (task.isSuccessful()){
                        Toast.makeText(AddAchivementActivity.this, "Prestasi berhasil ditambahkan", Toast.LENGTH_SHORT).show();
                        mProgresBar.setVisibility(View.INVISIBLE);
                        finish();
                    }else {
                        Log.d(TAG, task.getException().getLocalizedMessage());
                        Toast.makeText(AddAchivementActivity.this, "Prestasi gagal ditambahkan", Toast.LENGTH_SHORT).show();
                        mProgresBar.setVisibility(View.INVISIBLE);
                    }
                }
            });

        }

        return super.onOptionsItemSelected(item);
    }
}
