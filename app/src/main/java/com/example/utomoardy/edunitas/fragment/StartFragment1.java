package com.example.utomoardy.edunitas.fragment;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import com.example.utomoardy.edunitas.R;


/**
 * A simple {@link Fragment} subclass.
 */
public class StartFragment1 extends Fragment {

    Button btnNext;
    StartFragment2 frStart2 = new StartFragment2();

    public StartFragment1() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_start_fragment1, container, false);

        btnNext = rootView.findViewById(R.id.btn_start);

        btnNext.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                FragmentManager fragmentManager = getFragmentManager();
                FragmentTransaction fragmentTransaction=fragmentManager.beginTransaction();
                fragmentTransaction.replace( R.id.frame_layout,frStart2 );
                fragmentTransaction.commit();

                btnNext.setVisibility(View.GONE);
            }
        });
        // Inflate the layout for this fragment
        return rootView;


    }

}
