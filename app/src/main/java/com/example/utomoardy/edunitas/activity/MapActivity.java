package com.example.utomoardy.edunitas.activity;

import android.Manifest;
import android.app.ProgressDialog;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.location.Location;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.widget.TextView;
import android.widget.Toast;

import com.example.utomoardy.edunitas.DirectionFinder;
import com.example.utomoardy.edunitas.listener.DirectionFinderListener;
import com.example.utomoardy.edunitas.R;
import com.example.utomoardy.edunitas.TrackGPS;
import com.example.utomoardy.edunitas.module.Route;
import com.example.utomoardy.edunitas.module.School;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.Polyline;
import com.google.android.gms.maps.model.PolylineOptions;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;

public class MapActivity extends AppCompatActivity implements OnMapReadyCallback, LocationListener, DirectionFinderListener,
        GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener {

    FirebaseAuth mAuth;
    DatabaseReference mRootRef, mSchool;

    private GoogleMap mGoogleMap;
    private GoogleApiClient mGoogleApiClient;

    LocationRequest mLocationRequest;

    Location loc;
    School school = new School();

    private List<Marker> originMarkers = new ArrayList<>();
    private List<Marker> destinationMarkers = new ArrayList<>();
    private List<Polyline> polylinePaths = new ArrayList<>();
    private ProgressDialog progressDialog;

    String userId;

    private TrackGPS gps;

    String TAG = "MapActivity";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_map);

        //database
        mAuth = FirebaseAuth.getInstance();
        mRootRef = FirebaseDatabase.getInstance().getReference();
        mSchool = mRootRef.child("schools");

        //map
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.map_fragment);
        Log.d(TAG, "mapFragment" + mapFragment);
        mapFragment.getMapAsync(this);
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        Log.d(TAG, "GoogleMap : isReadyToUse");
        mGoogleMap = googleMap;

        userId = mAuth.getCurrentUser().getUid();

        final List<School> locationData = new ArrayList<>();
        mSchool.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                Log.d(TAG, "dataSnapshot :" + dataSnapshot);
                if (dataSnapshot.getValue() != null) {
                    Iterator<DataSnapshot> locationSnapshot = dataSnapshot.getChildren().iterator();
                    do {
                        HashMap<Double, Double> locationMap = new HashMap<>();
                        String schoolId = locationSnapshot.next().getKey();
                        school = dataSnapshot.child(schoolId).getValue(School.class);
                        double longitude = school.getLongitude();
                        double latitude = school.getLatitude();
                        Log.d(TAG, "latitude :" + latitude);
                        Log.d(TAG, "longitude :" + longitude);
                        LatLng tower = new LatLng(latitude, longitude);
                        mGoogleMap.addMarker(new MarkerOptions().position(tower));
                        mGoogleMap.moveCamera(CameraUpdateFactory.newLatLngZoom(tower, 20));
                        locationData.add(school);
                    } while (locationSnapshot.hasNext());
                } else {
                    Toast.makeText(getApplicationContext(), "Belum ada tower yang ditambahkan !!!", Toast.LENGTH_SHORT).show();
                }
            }//test1

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });

        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            return;
        }
        mGoogleMap.setMyLocationEnabled(true);
        mGoogleMap.setOnMarkerClickListener(new GoogleMap.OnMarkerClickListener() {
            @Override
            public boolean onMarkerClick(Marker marker) {
                Log.d(TAG, "markerOnClick");
                LatLng markerPosition = marker.getPosition();
                double lngTujuan = markerPosition.longitude;
                Log.d(TAG, "lngTujuan :" + lngTujuan);
                double latTujun = markerPosition.latitude;
                Log.d(TAG, "latTujuan :" + latTujun);
                sendRequest(loc, latTujun, lngTujuan);

                return true;
            }
        });
        mGoogleApiClient = new GoogleApiClient.Builder(this)
                .addApi(LocationServices.API)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .build();
        mGoogleApiClient.connect();

    }

    private void sendRequest(Location loc, double latTujun, double lngTujuan) {
        TrackGPS gps = new TrackGPS(MapActivity.this);
        Log.d(TAG, "latAwal :"+gps.getLatitude());
        String origin = gps.getLatitude() + "," + gps.getLongitude();
        String destination = latTujun + "," + lngTujuan;
        try {
            new DirectionFinder(this, origin, destination).execute();
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onLocationChanged(Location location) {
        if (location == null) {
            Toast.makeText(this, "Cant get current locatiob", Toast.LENGTH_LONG).show();
        } else {
            double latAwal = location.getLatitude();
            double lngAwal = location.getLongitude();
            Log.d(TAG, "latAwal :" + latAwal);
            Log.d(TAG, "lngAwal :" + lngAwal);
            LatLng ll = new LatLng(latAwal, lngAwal);
            CameraUpdate update = CameraUpdateFactory.newLatLngZoom(ll, 15);
            mGoogleMap.animateCamera(update);
        }
    }

    @Override
    public void onDirectionFinderStart() {
        progressDialog = ProgressDialog.show(this, "Harap Tunggu.",
                "Sedang mencari rute..!", true);

        if (polylinePaths != null) {
            for (Polyline polyline:polylinePaths ) {
                polyline.remove();
            }
        }
    }

    @Override
    public void onDirectionFinderSuccess(List<Route> routes) {
        progressDialog.dismiss();
        polylinePaths = new ArrayList<>();
        originMarkers = new ArrayList<>();
        destinationMarkers = new ArrayList<>();

        for (Route route : routes) {
            mGoogleMap.moveCamera(CameraUpdateFactory.newLatLngZoom(route.startLocation, 16));
            ((TextView) findViewById(R.id.tv_duration)).setText(route.duration.text);
            ((TextView) findViewById(R.id.tv_distance)).setText(route.distance.text);

            PolylineOptions polylineOptions = new PolylineOptions().
                    geodesic(true).
                    color(Color.BLUE).
                    width(10);

            for (int i = 0; i < route.points.size(); i++)
                polylineOptions.add(route.points.get(i));

            polylinePaths.add(mGoogleMap.addPolyline(polylineOptions));
        }
    }

    @Override
    public void onConnected(@Nullable Bundle bundle) {
        mLocationRequest = LocationRequest.create();
        mLocationRequest.setPriority(LocationRequest.PRIORITY_BALANCED_POWER_ACCURACY);

        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            return;
        }
        LocationServices.FusedLocationApi.requestLocationUpdates(mGoogleApiClient, mLocationRequest, this);
    }

    @Override
    public void onConnectionSuspended(int i) {

    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {

    }
}
