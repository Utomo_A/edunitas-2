package com.example.utomoardy.edunitas.module;

/**
 * Created by Utomo Ardy on 11/03/2018.
 */

public class School {
    String schoolName;
    String description;
    String achivementId;
    String agendaId;
    double longitude;
    double latitude;
    int rating;
    String requestId;
    String schoolImageUrl;

    public School() {
    }

    public School(String schoolName, String description, String achivementId, String agendaId, double longitude, double latitude, int rating, String requestId, String schoolImageUrl) {
        this.schoolName = schoolName;
        this.description = description;
        this.achivementId = achivementId;
        this.agendaId = agendaId;
        this.longitude = longitude;
        this.latitude = latitude;
        this.rating = rating;
        this.requestId = requestId;
        this.schoolImageUrl = schoolImageUrl;
    }

    public String getSchoolImageUrl() {
        return schoolImageUrl;
    }

    public void setSchoolImageUrl(String schoolImageUrl) {
        this.schoolImageUrl = schoolImageUrl;
    }

    public String getSchoolName() {
        return schoolName;
    }

    public void setSchoolName(String schoolName) {
        this.schoolName = schoolName;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getAchivementId() {
        return achivementId;
    }

    public void setAchivementId(String achivementId) {
        this.achivementId = achivementId;
    }

    public String getAgendaId() {
        return agendaId;
    }

    public void setAgendaId(String agendaId) {
        this.agendaId = agendaId;
    }

    public double getLongitude() {
        return longitude;
    }

    public void setLongitude(double longitude) {
        this.longitude = longitude;
    }

    public double getLatitude() {
        return latitude;
    }

    public void setLatitude(double latitude) {
        this.latitude = latitude;
    }

    public int getRating() {
        return rating;
    }

    public void setRating(int rating) {
        this.rating = rating;
    }

    public String getRequestId() {
        return requestId;
    }

    public void setRequestId(String requestId) {
        this.requestId = requestId;
    }
}

