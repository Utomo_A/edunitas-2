package com.example.utomoardy.edunitas.module;

import com.example.utomoardy.edunitas.module.Distance;
import com.example.utomoardy.edunitas.module.Duration;
import com.google.android.gms.maps.model.LatLng;

import java.util.List;

/**
 * Created by Utomo Ardy on 17/03/2018.
 */

public class Route {
    public Distance distance;
    public Duration duration;
    public String endAddress;
    public LatLng endLocation;
    public String startAddress;
    public LatLng startLocation;

    public List<LatLng> points;
}
