package com.example.utomoardy.edunitas.module;

import com.google.firebase.database.ServerValue;

/**
 * Created by windows on 3/15/2018.
 */

public class Comment {
    public String isiComment;
    public String fullname;
    public Object timestamp;

    public Comment() {
    }

    public Comment(String isiComment, String fullname) {
        this.isiComment = isiComment;
        this.fullname = fullname;
        this.timestamp = ServerValue.TIMESTAMP;
    }
}



