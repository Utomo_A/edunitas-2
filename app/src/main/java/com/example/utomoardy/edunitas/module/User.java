package com.example.utomoardy.edunitas.module;

/**
 * Created by Utomo Ardy on 10/03/2018.
 */

public class User {
    public String email;
    public String password;
    public String fullName;
    public String statusUser;
    public String aktif;
    public String verifikasi;
    public String photoprofile;
    public String schoolId;
    public String schoolName;
    public String uid;

    public User() {
    }

    public User(String email, String password, String fullName, String statusUser, String aktif, String verifikasi, String photoprofile, String schoolId, String schoolName, String uid) {
        this.email = email;
        this.password = password;
        this.fullName = fullName;
        this.statusUser = statusUser;
        this.aktif = aktif;
        this.verifikasi = verifikasi;
        this.photoprofile = photoprofile;
        this.schoolId = schoolId;
        this.schoolName = schoolName;
        this.uid = uid;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getFullName() {
        return fullName;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    public String getStatusUser() {
        return statusUser;
    }

    public void setStatusUser(String statusUser) {
        this.statusUser = statusUser;
    }

    public String getAktif() {
        return aktif;
    }

    public void setAktif(String aktif) {
        this.aktif = aktif;
    }

    public String getVerifikasi() {
        return verifikasi;
    }

    public void setVerifikasi(String verifikasi) {
        this.verifikasi = verifikasi;
    }
}
