package com.example.utomoardy.edunitas;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import com.example.utomoardy.edunitas.fragment.LoginFragment;
import com.example.utomoardy.edunitas.fragment.StartFragment1;
import com.example.utomoardy.edunitas.fragment.StartFragment2;

/**
 * Created by Utomo Ardy on 10/03/2018.
 */

public class SectionPagerAdapter extends FragmentPagerAdapter{
    public SectionPagerAdapter(FragmentManager fm) {
        super(fm);
    }

    @Override
    public Fragment getItem(int position) {

        switch (position){
            case 0:
                StartFragment1 startFragment1 = new StartFragment1();
                return startFragment1;
            case 1:
                StartFragment2 startFragment2 = new StartFragment2();
                return startFragment2;
            case 2:
                LoginFragment loginFragment = new LoginFragment();
                return loginFragment;
            default:
                return null;

        }
    }

    @Override
    public int getCount() {
        return 2;
    }
}
