package com.example.utomoardy.edunitas.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;


import com.example.utomoardy.edunitas.R;
import com.example.utomoardy.edunitas.module.Message;
import com.example.utomoardy.edunitas.viewHolder.MessageViewHolder;
import com.firebase.ui.database.FirebaseRecyclerAdapter;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.squareup.picasso.Picasso;

import de.hdodenhof.circleimageview.CircleImageView;

public class ChatRoomActivity extends AppCompatActivity {

    private static final String TAG = "Chat Room Activity" ;
    String nama, photo, uid;
    String currentUid;

    FirebaseAuth mAuth;
    FirebaseDatabase mDatabase;
    DatabaseReference mRootRef, mUserRef, mPostRef, mPretasiRef, mMessageRef;
    FirebaseStorage mStorage;
    StorageReference storageReference;

    CircleImageView circlePhoto;
    TextView tvNamaTeman;
    EditText etTyping;
    ImageView ivSend;

    RecyclerView mRecyclerView;
    FirebaseRecyclerAdapter<Message, MessageViewHolder> mAdapter;
    LinearLayoutManager mManager;

    String photoprofile, namaProfil;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_chat_room);

        mStorage = FirebaseStorage.getInstance();
        storageReference = mStorage.getReference();
        mAuth = FirebaseAuth.getInstance();
        mDatabase = FirebaseDatabase.getInstance();
        mRootRef = mDatabase.getReference();
        mUserRef = mRootRef.child("users");
        mPostRef = mRootRef.child("posts");
        mPretasiRef = mRootRef.child("prestasi");
        mMessageRef = mRootRef.child("messages");

        Intent intent = getIntent();
        nama = intent.getStringExtra("nama");
        photo = intent.getStringExtra("photo");
        uid = intent.getStringExtra("uid");

        circlePhoto = findViewById(R.id.circlePhotoProfileChatRoom);
        tvNamaTeman = findViewById(R.id.friendNAmeChatRoom);
        etTyping = findViewById(R.id.etTypingChatRoom);
        ivSend = findViewById(R.id.sendMessage);

        mRecyclerView = findViewById(R.id.recyclerview_chatRoom);
        mManager = new LinearLayoutManager(this.getApplicationContext());

        Picasso.with(getApplicationContext()).load(photo).into(circlePhoto);
        tvNamaTeman.setText(nama);

        ivSend.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                currentUid = mAuth.getCurrentUser().getUid();
                String text = etTyping.getText().toString().trim();
                if (text.length() > 0){
                    Message message = new Message(text, currentUid);
                    String push = mMessageRef.push().getKey();
                    mMessageRef.child(uid).child(currentUid).child(push).setValue(message);
                    mMessageRef.child(currentUid).child(uid).child(push).setValue(message);
                    etTyping.setText("");
                }

            }
        });

        mMessageRef.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                if (dataSnapshot.getChildren() != null) {
                    Log.d(TAG, "datasnapshot :" + dataSnapshot.getValue());
                    mRecyclerView.setLayoutManager(mManager);
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
    }

    @Override
    public void onResume() {
        super.onResume();

        if (mDatabase == null) {
            mDatabase = FirebaseDatabase.getInstance();
        }
        mRootRef = mDatabase.getReference();
        mUserRef = mRootRef.child("users");
        mMessageRef = mRootRef.child("messages");

        mAuth = FirebaseAuth.getInstance();
        currentUid = mAuth.getCurrentUser().getUid();

        mMessageRef.child(currentUid).child(uid).addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                if (dataSnapshot != null) {
//                    Log.d("data",dataSnapshot.child("message").getValue(String.class));
                    Log.d(TAG, "datasnapshot resume :" + dataSnapshot.getValue());
                    mAdapter = new FirebaseRecyclerAdapter<Message, MessageViewHolder>(
                            Message.class,
                            R.layout.item_chatroom,
                            MessageViewHolder.class,
                            mMessageRef.child(currentUid).child(uid)
                    ) {
                        @Override
                        protected void populateViewHolder(MessageViewHolder viewHolder, Message model, int position) {
                            final DatabaseReference postRef = getRef(position);
                            final String postKey = postRef.getKey();

                            viewHolder.bindToMessage(model, postKey);
                            Log.d(TAG,"Postkey : " + postKey);
                        }
                    };

                    mAdapter.registerAdapterDataObserver(new RecyclerView.AdapterDataObserver() {
                        @Override
                        public void onItemRangeInserted(int positionStart, int itemCount) {
                            super.onItemRangeInserted(positionStart, itemCount);
                            //Log.d(TAG, String.valueOf(itemCount));
                            mRecyclerView.smoothScrollToPosition(mAdapter.getItemCount());
                        }
                    });
                    mRecyclerView.setLayoutManager(mManager);
                    mRecyclerView.setAdapter(mAdapter);
                    mRecyclerView.getRecycledViewPool().clear();
                    mAdapter.notifyDataSetChanged();
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
    }
}
