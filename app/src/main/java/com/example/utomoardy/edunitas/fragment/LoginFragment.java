package com.example.utomoardy.edunitas.fragment;


import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.example.utomoardy.edunitas.R;
import com.example.utomoardy.edunitas.activity.MainActivity;
import com.example.utomoardy.edunitas.activity.RegisterActivity;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;


/**
 * A simple {@link Fragment} subclass.
 */
public class LoginFragment extends Fragment {

    FirebaseAuth mAuth;
    DatabaseReference mRootRef;

    EditText mEtEmail, mEtPassword;
    Button mBtnLogin, mBtnRegister, mBtnGuestLogin;
    ProgressBar mProgressBar;

    public LoginFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        final View rootView = inflater.inflate(R.layout.fragment_login, container, false);

        mAuth = FirebaseAuth.getInstance();
        mRootRef = FirebaseDatabase.getInstance().getReference();

        mEtEmail = rootView.findViewById(R.id.et_email);
        mEtPassword = rootView.findViewById(R.id.et_password);
        mProgressBar = rootView.findViewById(R.id.progress_bar);

        mBtnLogin = rootView.findViewById(R.id.btn_login);
        mBtnRegister = rootView.findViewById(R.id.btn_register);
        mBtnGuestLogin = rootView.findViewById(R.id.btn_login_guest);

        mBtnRegister.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getContext(), RegisterActivity.class);
                startActivity(intent);
            }
        });

        mBtnLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String email = mEtEmail.getText().toString();
                String password = mEtPassword.getText().toString();

                if (TextUtils.isEmpty(email)){
                    mEtEmail.setError("Email harus diisi");
                }else if (TextUtils.isEmpty(password)){
                    mEtPassword.setError("Kata sandi harus diisi");
                }else {
                    mProgressBar.setVisibility(View.VISIBLE);
                    mAuth.signInWithEmailAndPassword(email, password).addOnCompleteListener(new OnCompleteListener<AuthResult>() {
                        @Override
                        public void onComplete(@NonNull Task<AuthResult> task) {
                            if (task.isSuccessful()){
                                mProgressBar.setVisibility(View.INVISIBLE);
                                startActivity(new Intent(getContext(), MainActivity.class));

                            }else {
                                mProgressBar.setVisibility(View.INVISIBLE);
                                Toast.makeText(getContext(), task.getException().getLocalizedMessage(), Toast.LENGTH_LONG).show();
                            }
                        }
                    });
                }

            }
        });
        // Inflate the layout for this fragment
        return rootView;
    }

}
