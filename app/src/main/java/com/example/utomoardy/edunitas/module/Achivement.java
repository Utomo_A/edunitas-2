package com.example.utomoardy.edunitas.module;

/**
 * Created by Utomo Ardy on 12/03/2018.
 */

public class Achivement {
    String title;
    String description;

    public Achivement() {
    }

    public Achivement(String title, String description) {
        this.title = title;
        this.description = description;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}
