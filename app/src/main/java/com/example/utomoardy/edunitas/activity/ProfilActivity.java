package com.example.utomoardy.edunitas.activity;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.ListAdapter;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.example.utomoardy.edunitas.adapter.GridAchivementAdapter;
import com.example.utomoardy.edunitas.module.Achivement;
import com.example.utomoardy.edunitas.R;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;
import com.squareup.picasso.Picasso;
import com.theartofdev.edmodo.cropper.CropImage;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;

public class ProfilActivity extends AppCompatActivity {

    FirebaseAuth mAuth;
    FirebaseUser mCurrentUser;
    DatabaseReference mRootRef, mSchool, mUser, mAchivement;
    private StorageReference mImageStorage;

    private static final int GALLERY_PICK = 1;

    TextView mTvSchoolName, mTvSchoolDescription;
    ImageView mImgChangeProfile, mImgProfile, mImgAddAchivements;

    ProgressBar mProgressBar;

    GridView gvAchivment;

    private String TAG = "ProfilActivity";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profil);
        getSupportActionBar().setTitle("Profil");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        mAuth = FirebaseAuth.getInstance();
        mCurrentUser = FirebaseAuth.getInstance().getCurrentUser();
        mRootRef = FirebaseDatabase.getInstance().getReference();
        String userId = mCurrentUser.getUid();
        mUser = mRootRef.child("users").child(userId);
        mSchool = mRootRef.child("schools");
        mAchivement = mRootRef.child("achivements");
        mImageStorage = FirebaseStorage.getInstance().getReference();

        mProgressBar = findViewById(R.id.progress_bar);

        gvAchivment = findViewById(R.id.grid_view);

        mTvSchoolName = findViewById(R.id.tv_school_name);
        mTvSchoolDescription = findViewById(R.id.tv_description_content);
        mImgChangeProfile = findViewById(R.id.image_change_picture);
        mImgProfile = findViewById(R.id.iv_school_profile);
        mImgAddAchivements = findViewById(R.id.image_add_achivement);

        mImgAddAchivements.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(ProfilActivity.this, AddAchivementActivity.class);
                startActivity(intent);
            }
        });

        mImgChangeProfile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent galerry_intent = new Intent();
                galerry_intent.setType("image/*");
                galerry_intent.setAction(Intent.ACTION_GET_CONTENT);
                startActivityForResult(Intent.createChooser(galerry_intent, "SELECT IMAGE"), GALLERY_PICK);
            }
        });

        setValue();

    }

    private void setValue() {
        String userId = mAuth.getCurrentUser().getUid();

        mSchool.child(userId).addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                String name = dataSnapshot.child("schoolName").getValue().toString();
                String description = dataSnapshot.child("description").getValue().toString();
                String image = dataSnapshot.child("schoolImageUrl").getValue().toString();
//                String achivementId = dataSnapshot.child("achivementId").getValue().toString();

                mTvSchoolName.setText(name);
                mTvSchoolDescription.setText(description);

                if (image.equals("")){
                    Log.d(TAG, "foto belum ditambahkan");
                }else {
                    Picasso.with(ProfilActivity.this)
                            .load(image).into(mImgProfile);
                }

                fetchLocation();
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
    }

    private void fetchLocation() {
        final List <Achivement> achivementData = new ArrayList<>();
        String adminSchoolId = mAuth.getCurrentUser().getUid();

        if (mAchivement.child(adminSchoolId) != null){
            mAchivement.child(adminSchoolId).addValueEventListener(new ValueEventListener() {
                @Override
                public void onDataChange(DataSnapshot dataSnapshot) {
                    Log.d(TAG, "dataSnapshot :"+ dataSnapshot);
                    Iterator<DataSnapshot> achivementSnapshot = dataSnapshot.getChildren().iterator();
                    do {
                        HashMap <String, String> achivementMap = new HashMap<String, String>();
                        Achivement achivement = new Achivement();
                        String locId = achivementSnapshot.next().getKey();
                        Log.d(TAG, "locId :"+locId);
                        achivement = dataSnapshot.child(locId).getValue(Achivement.class);
                        Log.d(TAG, "achivement title :"+achivement.getTitle());
                        Log.d(TAG, "achivement description :"+achivement.getDescription());
                        achivementData.add(achivement);

                    }while (achivementSnapshot.hasNext());
                    GridAchivementAdapter adapter = new GridAchivementAdapter(achivementData, ProfilActivity.this);
                    gvAchivment.setAdapter(adapter);
                    setGridViewHeightBasedOnChildren(gvAchivment,3);
                }

                @Override
                public void onCancelled(DatabaseError databaseError) {

                }
            });

        }else {
            Log.d(TAG, "prestasi belum ditambahkan");
        }

    }

    public void setGridViewHeightBasedOnChildren(GridView gridView, int columns) {
        ListAdapter listAdapter = gridView.getAdapter();
        if (listAdapter == null) {
            // pre-condition
            return;
        }

        int totalHeight = 0;
        int items = listAdapter.getCount();
        int rows = 0;

        View listItem = listAdapter.getView(0, null, gridView);
        listItem.measure(0, 0);
        totalHeight = listItem.getMeasuredHeight();

        float x = 1;
        if( items > columns ){
            x = items/columns;
            rows = (int) (x + 1);
            totalHeight *= rows;
        }

        ViewGroup.LayoutParams params = gridView.getLayoutParams();
        params.height = totalHeight;
        gridView.setLayoutParams(params);

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == GALLERY_PICK && resultCode == RESULT_OK) {
            Uri imageUri = data.getData();

            CropImage.activity(imageUri)
                    .start(this);
        }

        if (requestCode == CropImage.CROP_IMAGE_ACTIVITY_REQUEST_CODE) {
            CropImage.ActivityResult result = CropImage.getActivityResult(data);
            if (resultCode == RESULT_OK) {

                mProgressBar.setVisibility(View.VISIBLE);

                Uri resultUri = result.getUri();

                Log.d(TAG, "resultUri :"+resultUri);

                String current_userId = mCurrentUser.getUid();
                StorageReference filepath = mImageStorage.child("profile_images").child(current_userId + ".jpg");
                filepath.putFile(resultUri).addOnCompleteListener(new OnCompleteListener<UploadTask.TaskSnapshot>() {
                    @Override
                    public void onComplete(@NonNull Task<UploadTask.TaskSnapshot> task) {

                        if (task.isSuccessful()) {
                            String download_url = task.getResult().getDownloadUrl().toString();

                            String userId = mCurrentUser.getUid();

                            Log.d(TAG, "download_url :"+download_url);

                            mSchool.child(userId).child("schoolImageUrl").setValue(download_url).addOnCompleteListener(new OnCompleteListener<Void>() {
                                @Override
                                public void onComplete(@NonNull Task<Void> task) {
                                    if (task.isSuccessful()){
                                        mProgressBar.setVisibility(View.INVISIBLE);
                                        Toast.makeText(ProfilActivity.this, "Upload berhasil", Toast.LENGTH_LONG).show();
                                    }else {
                                        mProgressBar.setVisibility(View.INVISIBLE);
                                        Toast.makeText(ProfilActivity.this, "Upload gagal", Toast.LENGTH_LONG).show();
                                        Log.d(TAG, "Upload database gagal :"+ task.getException().getLocalizedMessage());
                                    }
                                }
                            });
                        } else {
                            Log.d(TAG, "Upload storage gagal :"+ task.getException().getLocalizedMessage());
                            Toast.makeText(ProfilActivity.this, "Upload gagal", Toast.LENGTH_LONG).show();
                            mProgressBar.setVisibility(View.INVISIBLE);
                        }

                    }
                });

            } else if (resultCode == CropImage.CROP_IMAGE_ACTIVITY_RESULT_ERROR_CODE) {
                Exception error = result.getError();
            }
        }

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {

        getMenuInflater().inflate(R.menu.profil_menu,menu);

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        Log.d("ProfilActivity",  String.valueOf(id));

        if (id==R.id.item_setting_profil){
            Log.d("ProfilActivity",  String.valueOf(id));
            startActivity(new Intent(ProfilActivity.this, EditProfilActivity.class));
        }

        return super.onContextItemSelected(item);
    }
}
