package com.example.utomoardy.edunitas.module;

/**
 * Created by windows on 3/20/2018.
 */

public class Prestasi {
    public String juara;
    public String keterangan;

    public Prestasi() {
    }

    public Prestasi(String juara, String keterangan) {
        this.juara = juara;
        this.keterangan = keterangan;
    }
}
