package com.example.utomoardy.edunitas.viewHolder;

import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.RatingBar;
import android.widget.TextView;

import com.example.utomoardy.edunitas.R;
import com.example.utomoardy.edunitas.module.School;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.squareup.picasso.Picasso;

import de.hdodenhof.circleimageview.CircleImageView;

/**
 * Created by Utomo Ardy on 24/03/2018.
 */

public class SearchViewHolder extends RecyclerView.ViewHolder {
    FirebaseAuth mAuth;
    DatabaseReference mRootRef, mSchoolRef;

    TextView tvSchoolName;
    RatingBar ratingBar;
    CircleImageView imageSchool;

    School displayedMessage = new School();
    public String postKey;

    String TAG = "SearchViewHolder";

    public SearchViewHolder(View itemView) {
        super(itemView);

        mAuth = FirebaseAuth.getInstance();
        mRootRef = FirebaseDatabase.getInstance().getReference();
        mSchoolRef = mRootRef.child("schools");

        tvSchoolName = itemView.findViewById(R.id.tv_school_name);
        ratingBar = itemView.findViewById(R.id.rating_bar);
        imageSchool = itemView.findViewById(R.id.image_school);
    }

    public void bindToPost(final School school, String postKey, String searchSchoolName){
        this.postKey = postKey;



        displayedMessage = school;
        Log.d(TAG, "searchSchoolName :"+searchSchoolName);
        mSchoolRef.orderByChild("schoolName").equalTo(searchSchoolName);
        mSchoolRef.child(postKey).addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                if (dataSnapshot.getValue() == null){
                    Log.d(TAG, "sekolah tidak ditemukan");
                }else {
                    Log.d(TAG, "dataSnapshot :"+dataSnapshot);
                    String schoolName = dataSnapshot.child("schoolName").getValue().toString();
                    String imageUrl = dataSnapshot.child("schoolImageUrl").getValue().toString();
                    int rating = Integer.parseInt(dataSnapshot.child("rating").getValue().toString());
                    Log.d(TAG, "rating :"+rating);

                    tvSchoolName.setText(schoolName);
                    Picasso.with(itemView.getContext())
                            .load(imageUrl)
                            .into(imageSchool);
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
    }
}
