package com.example.utomoardy.edunitas.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.example.utomoardy.edunitas.R;
import com.example.utomoardy.edunitas.module.Achivement;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Utomo Ardy on 13/03/2018.
 */

public class GridAchivementAdapter extends BaseAdapter {
    private List<Achivement> achivement = new ArrayList<>();
    private final Context context;

    public GridAchivementAdapter(List<Achivement> achivement, Context context) {
        this.achivement = achivement;
        this.context = context;
    }

    @Override
    public int getCount() {
        return achivement.size();
    }

    @Override
    public Object getItem(int i) {
        return achivement.get(i);
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {

        if (view == null){
            view = LayoutInflater.from(context).inflate(R.layout.grid_item_achivement, viewGroup, false);
        }
        TextView tvTitle = (TextView)view.findViewById(R.id.tv_achivement_title);
        TextView tvDescription = (TextView)view.findViewById(R.id.tv_achivement_description);

        Achivement achivement = (Achivement) getItem(i);

        tvTitle.setText(achivement.getTitle());
        tvDescription.setText(achivement.getDescription());

        return view;
    }
}
