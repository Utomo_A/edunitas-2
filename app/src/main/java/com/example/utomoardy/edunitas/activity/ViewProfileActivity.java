package com.example.utomoardy.edunitas.activity;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.ListAdapter;
import android.widget.TextView;

import com.example.utomoardy.edunitas.R;
import com.example.utomoardy.edunitas.adapter.GridAchivementAdapter;
import com.example.utomoardy.edunitas.module.Achivement;
import com.example.utomoardy.edunitas.module.School;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;

public class ViewProfileActivity extends AppCompatActivity {

    FirebaseAuth mAuth;
    DatabaseReference mRootRef, mSchoolRef, mAchivementRef;

    TextView mTvSchoolDescription, mTvSchoolName;
    ImageView mIvProfileSchool;

    GridView gvAchivment;

    String mSchoolId;
    String TAG = "ViewProfileActivity";

    School mSchool = new School();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_view_profile);
        getSupportActionBar().setTitle("Profil");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        Bundle extras = getIntent().getExtras();
        if (extras != null){
            mSchoolId = extras.getString("post_key");
        }

        mAuth = FirebaseAuth.getInstance();
        mRootRef = FirebaseDatabase.getInstance().getReference();
        mSchoolRef = mRootRef.child("schools");
        mAchivementRef = mRootRef.child("achivements");

        gvAchivment = findViewById(R.id.grid_view);

        mTvSchoolDescription = findViewById(R.id.tv_school_description);
        mTvSchoolName = findViewById(R.id.tv_school_name);
        mIvProfileSchool = findViewById(R.id.iv_school_profile);

        Log.d(TAG, "postKey :"+ mSchoolId);
        mSchoolRef.child(mSchoolId).addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                Log.d(TAG, "dataSnapshot :"+dataSnapshot);
                String schoolName, schoolDescription, schoolImageUrl, achivementId;
                mSchool = dataSnapshot.getValue(School.class);
                schoolName = mSchool.getSchoolName();
                schoolDescription = mSchool.getDescription();
                schoolImageUrl = mSchool.getSchoolImageUrl();
                achivementId = mSchool.getAchivementId();
                Log.d(TAG, "schoolName :"+schoolName);
                Log.d(TAG, "schoolDescription :"+schoolDescription);
                Log.d(TAG, "schoolImageUrl :"+schoolImageUrl);
                mTvSchoolDescription.setText(schoolDescription);
                mTvSchoolName.setText(schoolName);
                Picasso.with(ViewProfileActivity.this)
                        .load(schoolImageUrl)
                        .into(mIvProfileSchool);
                setAchivement();
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });

    }

    private void setAchivement() {
        final List<Achivement> achivementData = new ArrayList<>();

        if (mAchivementRef.child(mSchoolId) != null){
            mAchivementRef.child(mSchoolId).addValueEventListener(new ValueEventListener() {
                @Override
                public void onDataChange(DataSnapshot dataSnapshot) {
                    Log.d(TAG, "dataSnapshot :"+ dataSnapshot);
                    Iterator<DataSnapshot> achivementSnapshot = dataSnapshot.getChildren().iterator();
                    do {
                        HashMap<String, String> achivementMap = new HashMap<String, String>();
                        Achivement achivement = new Achivement();
                        String locId = achivementSnapshot.next().getKey();
                        Log.d(TAG, "locId :"+locId);
                        achivement = dataSnapshot.child(locId).getValue(Achivement.class);
                        Log.d(TAG, "achivement title :"+achivement.getTitle());
                        Log.d(TAG, "achivement description :"+achivement.getDescription());
                        achivementData.add(achivement);

                    }while (achivementSnapshot.hasNext());
                    GridAchivementAdapter adapter = new GridAchivementAdapter(achivementData, ViewProfileActivity.this);
                    gvAchivment.setAdapter(adapter);
                    setGridViewHeightBasedOnChildren(gvAchivment,3);
                }

                @Override
                public void onCancelled(DatabaseError databaseError) {

                }
            });

        }else {
            Log.d(TAG, "prestasi belum ditambahkan");
        }

    }

    public void setGridViewHeightBasedOnChildren(GridView gridView, int columns) {
        ListAdapter listAdapter = gridView.getAdapter();
        if (listAdapter == null) {
            // pre-condition
            return;
        }

        int totalHeight = 0;
        int items = listAdapter.getCount();
        int rows = 0;

        View listItem = listAdapter.getView(0, null, gridView);
        listItem.measure(0, 0);
        totalHeight = listItem.getMeasuredHeight();

        float x = 1;
        if( items > columns ){
            x = items/columns;
            rows = (int) (x + 1);
            totalHeight *= rows;
        }

        ViewGroup.LayoutParams params = gridView.getLayoutParams();
        params.height = totalHeight;
        gridView.setLayoutParams(params);

    }
}
