package com.example.utomoardy.edunitas.activity;

import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationListener;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.example.utomoardy.edunitas.R;
import com.example.utomoardy.edunitas.TrackGPS;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.io.IOException;
import java.util.List;

public class EditProfilActivity extends AppCompatActivity implements LocationListener {
    FirebaseAuth mAuth;
    DatabaseReference mRootRef, mSchool;

    ImageView mImageFindLocation;
    TextView mTvLocation;
    EditText mEtSchoolName, mEtSchoolDescription;
    ProgressBar mProgressBar;

    private TrackGPS gps;

    String mAddress;

    String TAG = "EditProfileActivity";

    double mLongitude, mLatitude;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_profil);
        getSupportActionBar().setTitle("Pengaturan profil");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        gps = new TrackGPS(EditProfilActivity.this);

        mAuth = FirebaseAuth.getInstance();
        mRootRef = FirebaseDatabase.getInstance().getReference();
        mSchool = mRootRef.child("schools");

        mImageFindLocation = findViewById(R.id.image_find_location);
        mTvLocation = findViewById(R.id.tv_location);
        mEtSchoolName = findViewById(R.id.et_school_name);
        mEtSchoolDescription = findViewById(R.id.et_school_description);

        mProgressBar = findViewById(R.id.progress_bar);

        mImageFindLocation.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Log.d(TAG, "findLocation : onClicked");
                if (gps.canGetLocation()) {
                    try {
                        Log.d(TAG, "latitude : "+gps.getLatitude());
                        mLatitude = gps.getLatitude();
                        mLongitude = gps.getLongitude();
                        Log.d(TAG, "longitude : "+mLongitude);
                        Geocoder gc = new Geocoder(EditProfilActivity.this);
                        List<Address> list = gc.getFromLocation(mLatitude, mLongitude, 1);
                        Log.d(TAG, "list :"+list);
                        mAddress = list.get(0).getAddressLine(0);
                        Log.d(TAG, "mAddress :"+ mAddress);
                        mTvLocation.setText(mAddress);
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                } else {
                    gps.showSettingAlert();
                }
            }
        });

        getValue();

    }

    private void getValue() {
        String userId = mAuth.getCurrentUser().getUid();
        mSchool.child(userId).addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                String name = dataSnapshot.child("schoolName").getValue().toString();
                String description = dataSnapshot.child("description").getValue().toString();
                Log.d("EditProfile Activity", "schoolName :"+name);
                Log.d("EditProfile Activity", "dataSnapshot:"+dataSnapshot);

                mEtSchoolName.setText(name);
                mEtSchoolDescription.setText(description);
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.setting_menu,menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        int id = item.getItemId();

        if (id == R.id.item_done){
            String name = mEtSchoolName.getText().toString();
            final String description = mEtSchoolDescription.getText().toString();

            final String userId = mAuth.getCurrentUser().getUid();

            mProgressBar.setVisibility(View.VISIBLE);

            mSchool.child(userId).child("description").setValue(description);
            mSchool.child(userId).child("longitude").setValue(mLongitude);
            mSchool.child(userId).child("latitude").setValue(mLatitude);
            mSchool.child(userId).child("address").setValue(mAddress);
            mSchool.child(userId).child("schoolName").setValue(name).addOnCompleteListener(new OnCompleteListener<Void>() {
                @Override
                public void onComplete(@NonNull Task<Void> task) {
                    if (task.isSuccessful()){
                        mProgressBar.setVisibility(View.INVISIBLE);
                        Toast.makeText(EditProfilActivity.this, "Data berhasil disimpan", Toast.LENGTH_SHORT).show();
                        finish();
                    }else {
                        mProgressBar.setVisibility(View.INVISIBLE);
                        Toast.makeText(EditProfilActivity.this, "Gagal menyimpan data", Toast.LENGTH_LONG).show();
                    }
                }
            });
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onLocationChanged(final Location location) {

    }

    @Override
    public void onStatusChanged(String s, int i, Bundle bundle) {

    }

    @Override
    public void onProviderEnabled(String s) {

    }

    @Override
    public void onProviderDisabled(String s) {

    }
}
