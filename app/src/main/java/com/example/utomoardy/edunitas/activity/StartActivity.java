package com.example.utomoardy.edunitas.activity;

import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.FrameLayout;

import com.example.utomoardy.edunitas.R;
import com.example.utomoardy.edunitas.fragment.LoginFragment;
import com.example.utomoardy.edunitas.fragment.StartFragment1;
import com.example.utomoardy.edunitas.fragment.StartFragment2;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;

public class StartActivity extends AppCompatActivity {
    Button mBtnNext;
    String TAG = "StartActivity";

    FirebaseAuth mAuth;
    FirebaseAuth.AuthStateListener mAuthListener;

    private FrameLayout mFrameLayout;
    ViewPager viewPager;

    StartFragment1 frStart1 = new StartFragment1();
    LoginFragment frLogin = new LoginFragment();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_start);

        FragmentManager fragmentManager = getSupportFragmentManager();
        FragmentTransaction fragmentTransaction=fragmentManager.beginTransaction();
        fragmentTransaction.replace( R.id.frame_layout,frStart1);
        fragmentTransaction.commit();

        mFrameLayout = findViewById(R.id.frame_layout);

        mAuth = FirebaseAuth.getInstance();
        mAuthListener = new FirebaseAuth.AuthStateListener() {
            @Override
            public void onAuthStateChanged(@NonNull FirebaseAuth firebaseAuth) {
                FirebaseUser user = firebaseAuth.getCurrentUser();
                if (user != null){
                    startActivity(new Intent(StartActivity.this, MainActivity.class));
                    Log.d(TAG, "onAuthStateChanged:signed_in");
                    finish();
                }else {
                    Log.d(TAG, "onAuthStateChanged:signed_out: ");
                }
            }
        };


//        btnStart = findViewById(R.id.btn_start);

    }

    @Override
    public void onStart() {
        super.onStart();
        mAuth.addAuthStateListener(mAuthListener);
    }
    @Override
    public void onStop() {
        super.onStop();
        if (mAuthListener != null) {
            mAuth.removeAuthStateListener(mAuthListener);
        }
    }
}
