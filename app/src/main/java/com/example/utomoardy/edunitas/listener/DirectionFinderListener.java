package com.example.utomoardy.edunitas.listener;

import com.example.utomoardy.edunitas.module.Route;

import java.util.List;

/**
 * Created by Utomo Ardy on 17/03/2018.
 */

public interface DirectionFinderListener {
    void onDirectionFinderStart();
    void onDirectionFinderSuccess(List<Route> routes);
}
