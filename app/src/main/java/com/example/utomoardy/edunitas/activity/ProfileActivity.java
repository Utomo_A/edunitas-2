package com.example.utomoardy.edunitas.activity;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.widget.TextView;

import com.example.utomoardy.edunitas.R;
import com.example.utomoardy.edunitas.module.Prestasi;
import com.example.utomoardy.edunitas.viewHolder.PrestasiViewHolder;
import com.firebase.ui.database.FirebaseRecyclerAdapter;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.squareup.picasso.Picasso;

import de.hdodenhof.circleimageview.CircleImageView;

public class ProfileActivity extends AppCompatActivity {

    CircleImageView circlePhotoProfile;
    TextView tvNamaProfile, tvAsalSekolah;

    FirebaseAuth mAuth;
    FirebaseDatabase mDatabase;
    DatabaseReference mRootRef, mUserRef, mPostRef, mPrestasiRef;
    FirebaseStorage mStorage;
    StorageReference storageReference;

    RecyclerView mRecyclerView;
    FirebaseRecyclerAdapter<Prestasi, PrestasiViewHolder> mAdapter;
    LinearLayoutManager mManager;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile);

        circlePhotoProfile = findViewById(R.id.circlePhotoProfile);
        tvNamaProfile = findViewById(R.id.tvNamaProfile);
        tvAsalSekolah = findViewById(R.id.tvAsalSekolah);

        mStorage = FirebaseStorage.getInstance();
        storageReference = mStorage.getReference();
        mAuth = FirebaseAuth.getInstance();
        mDatabase = FirebaseDatabase.getInstance();
        mRootRef = mDatabase.getReference();
        mUserRef = mRootRef.child("users");
        mPostRef = mRootRef.child("posts");
        mPrestasiRef = mRootRef.child("prestasi");

        mRecyclerView = findViewById(R.id.pretasi_recyclerview);
        mManager = new LinearLayoutManager(this.getApplicationContext(), LinearLayoutManager.HORIZONTAL,true);

        mManager.setReverseLayout(true);
        mManager.setStackFromEnd(true);

        String uid = mAuth.getCurrentUser().getUid();

        mPrestasiRef.child(uid).addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                if (dataSnapshot.getChildren() != null) {
                    Log.d("PrestasiActivity", "datasnapshot :" + dataSnapshot.getValue());
                    mRecyclerView.setLayoutManager(mManager);
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });

        mUserRef.child(uid).addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                String photo = dataSnapshot.child("photoprofile").getValue(String.class);
                String sekolah = dataSnapshot.child("sekolah").getValue(String.class);
                String nama = dataSnapshot.child("fullName").getValue(String.class);
                Picasso.with(getApplicationContext()).load(photo).into(circlePhotoProfile);
                tvNamaProfile.setText(nama);
                tvAsalSekolah.setText(sekolah);
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });

    }

    @Override
    public void onResume() {
        super.onResume();

        if (mDatabase == null) {
            mDatabase = FirebaseDatabase.getInstance();
        }
        mRootRef = mDatabase.getReference();
        mUserRef = mRootRef.child("users");
        mPostRef = mRootRef.child("posts");
        mPrestasiRef = mRootRef.child("prestasi");

        mAuth = FirebaseAuth.getInstance();

        String uid = mAuth.getCurrentUser().getUid();

        mAdapter = new FirebaseRecyclerAdapter<Prestasi, PrestasiViewHolder>(
                Prestasi.class,
                R.layout.item_prestasi,
                PrestasiViewHolder.class,
                mPrestasiRef.child(uid)
        ) {
            @Override
            protected void populateViewHolder(PrestasiViewHolder viewHolder, Prestasi model, int position) {
                final DatabaseReference postRef = getRef(position);
                final String postKey = postRef.getKey();

                viewHolder.bindToPublic(model, postKey);
                Log.d("PrestasiActivity","Postkey : " + postKey);
            }
        };

        mAdapter.registerAdapterDataObserver(new RecyclerView.AdapterDataObserver() {
            @Override
            public void onItemRangeInserted(int positionStart, int itemCount) {
                super.onItemRangeInserted(positionStart, itemCount);
                mRecyclerView.smoothScrollToPosition(mAdapter.getItemCount());
            }
        });

        mPostRef.child("public").addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                if (dataSnapshot.getChildren() != null) {
                    Log.d("PrestasiActivity", "datasnapshot resume :" + dataSnapshot.getValue());
                    mRecyclerView.setLayoutManager(mManager);
                    mRecyclerView.setAdapter(mAdapter);
                    mRecyclerView.getRecycledViewPool().clear();
                    mAdapter.notifyDataSetChanged();
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });

    }

}

